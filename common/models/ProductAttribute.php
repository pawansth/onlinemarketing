<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_attribute".
 *
 * @property int $id
 * @property string $name
 * @property int $filter 
 * @property int $range 
 * @property int $row
 * @property int $cat_id
 * @property int $parent
 * @property int $status
 *
 * @property ProductAttValue[] $productAttValues
 * @property Categories $cat
 */
class ProductAttribute extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'product_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'cat_id'], 'required'],
            [['filter', 'range', 'row', 'cat_id', 'parent', 'status'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['cat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'cat_id' => 'Categories Name',
            'filter' => 'Filter',
            'range' => 'Range',
            'row' => 'Row',
            'parent' => 'Parent',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttValues() {
        return $this->hasMany(ProductAttValue::className(), ['prod_att_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat() {
        return $this->hasOne(Categories::className(), ['id' => 'cat_id']);
    }

}
