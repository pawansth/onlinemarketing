<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "img_aids".
 *
 * @property int $id
 * @property string $path
 * @property string $link
 * @property int $status
 */
class ImgAids extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'img_aids';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['path', 'link'], 'required'],
            [['status'], 'integer'],
            [['path'], 'file', 'extensions' => 'png,jpg,gif,jpeg'],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'link' => 'Link',
            'status' => 'Status',
        ];
    }
}
