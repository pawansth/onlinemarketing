<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property string $status
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    public $captcha;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            [['name', 'email'], 'string', 'max' => 100],
            [['message'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 20],
            [['captcha'], 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'message' => 'Message',
            'status' => 'Advertisement',
        ];
    }
    
    
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        $subject = 'Contact';
        if($this->status === 'advertisement') {
            $subject = 'Advertisement Request';
        }
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($subject)
            ->setTextBody($this->message)
            ->send();
    }
    
}
