<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $price
 *@property string $description
 * @property string $release_date
 * @property int $cat_id
 * @property int $sub_cat_id
 * @property int $brand_id
 * @property int $status
 *
 * @property Brand $brand
 * @property Categories $cat
 * @property Categories $subCat
 * @property ProductAdvertiseCode[] $productAdvertiseCodes
 * @property ProductAttValue[] $productAttValues
 * @property ProductImg[] $productImgs
 * @property ProductRating[] $productRatings
 * @property ProductReview[] $productReviews
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price', 'cat_id', 'brand_id'], 'required'],
            [['description'], 'string'],
            [['release_date'], 'safe'],
            [['cat_id', 'sub_cat_id', 'brand_id', 'status'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['price'], 'string', 'max' => 50],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['cat_id' => 'id']],
            [['sub_cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['sub_cat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'description' => 'Description',
            'release_date' => 'Release Date',
            'cat_id' => 'Categories Name',
            'sub_cat_id' => 'Sub Categories Name',
            'brand_id' => 'Brand Name',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(Categories::className(), ['id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCat()
    {
        return $this->hasOne(Categories::className(), ['id' => 'sub_cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAdvertiseCodes()
    {
        return $this->hasMany(ProductAdvertiseCode::className(), ['prod_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttValues()
    {
        return $this->hasMany(ProductAttValue::className(), ['prod_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImgs()
    {
        return $this->hasMany(ProductImg::className(), ['prod_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRatings()
    {
        return $this->hasMany(ProductRating::className(), ['prod_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductReviews()
    {
        return $this->hasMany(ProductReview::className(), ['prod_id' => 'id']);
    }
}
