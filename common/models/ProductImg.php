<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_img".
 *
 * @property int $id
 * @property int $prod_id
 * @property string $path
 * @property int $status
 *
 * @property Product $prod
 */
class ProductImg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_img';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prod_id'], 'required'],
            [['path'], 'safe'],
            [['prod_id', 'status'], 'integer'],
            [['path'], 'file', 'extensions' => 'png,jpg,gif,jpeg'],
            [['prod_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['prod_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Product Name',
            'path' => 'Product Image',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(Product::className(), ['id' => 'prod_id']);
    }
}
