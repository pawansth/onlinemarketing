<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "product_review".
 *
 * @property int $id
 * @property int $prod_id
 * @property string $review
 * @property string $review_date
 * @property string $email
 * @property int $status
 * 
 * @property Product $prod
 */
class ProductReview extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    public $captcha;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prod_id', 'name', 'review', 'captcha', 'review_date', 'email'], 'required'],
            [['prod_id', 'status'], 'integer'],
            [['review_date'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['review'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['captcha'], 'captcha'],
            [['prod_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['prod_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Product Name',
            'name' => 'Full Name',
            'review' => 'Review',
            'review_date' => 'Review Date',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(Product::className(), ['id' => 'prod_id']);
    }
}
