<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "key_des".
 *
 * @property int $id
 * @property string $keyword
 * @property string $description
 * @property string $ind
 * @property int $target_id
 */
class KeyDes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'key_des';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keyword', 'description', 'ind'], 'required'],
            [['keyword', 'description'], 'string'],
            [['target_id'], 'integer'],
            [['ind'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword' => 'Keyword',
            'description' => 'Description',
            'ind' => 'Ind',
            'target_id' => 'Target ID',
        ];
    }
}
