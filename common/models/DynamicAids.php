<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dynamic_aids".
 *
 * @property int $id
 * @property string $code
 * @property string $size
 * @property int $status
 */
class DynamicAids extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dynamic_aids';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'size'], 'required'],
            [['code'], 'string'],
            [['status'], 'integer'],
            [['size'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'size' => 'Size',
            'status' => 'Status',
        ];
    }
}
