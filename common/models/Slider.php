<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $path
 * @property string $link
 * @property string $title
 * @property string $description
 * @property int $status
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['path', 'link', 'title'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['path'], 'file', 'extensions' => 'png,jpg,gif,jpeg'],
            [['link'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Image',
            'link' => 'Link',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
}
