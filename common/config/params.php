<?php
return [
    'adminEmail' => 'asisg.1900@gmail.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
