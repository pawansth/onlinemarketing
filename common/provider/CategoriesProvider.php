<?php

/**
 * Description of CategoriesProvider
 *
 * @author pawan
 */

namespace common\provider;

use common\models\Categories;

class CategoriesProvider {
    static public function categoriesList() {
        $data = Categories::find()->asArray()->all();
        
        foreach($data as $key => &$value) {
            $output[$value['id']] = &$value;
        }
        
        foreach($data as $key => &$value) {
            if($value['parent'] && isset($output[$value['parent']])) {
                $node['node'] = &$value;
                array_push($output[$value['parent']], $node);
            }
        }
        
        foreach($data as $key => &$value) {
            if($value['parent'] && isset($output[$value['parent']])) {
                unset($data[$key]);
            }
        }
    
        return $data;
        
    }
    
}
