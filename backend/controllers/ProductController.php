<?php

namespace backend\controllers;

use Yii;
use common\models\Product;
use backend\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Categories;
use common\models\Brand;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Product();
        $modelImg = new \common\models\ProductImg();
        $modelKeyDes = new \common\models\KeyDes();

//        if (Yii::$app->request->post()) {
//            print_r(Yii::$app->request->post());
//            die();

        if (Yii::$app->request->post('Product')) {
            $model->name = Yii::$app->request->post('Product')['name'];
            $model->price = Yii::$app->request->post('Product')['price'];
            $model->description = Yii::$app->request->post('Product')['description'];
            $model->release_date = Yii::$app->request->post('Product')['release_date'];
            $model->cat_id = Yii::$app->request->post('Product')['cat_id'];
            $model->sub_cat_id = Yii::$app->request->post('Product')['sub_cat_id'];
            $model->brand_id = Yii::$app->request->post('Product')['brand_id'];
            $model->save();
            if (Yii::$app->request->post('KeyDes')) {
                $modelKeyDes->keyword = Yii::$app->request->post('KeyDes')['keyword'];
                $modelKeyDes->description = Yii::$app->request->post('KeyDes')['description'];
                $modelKeyDes->ind = 'product';
                $modelKeyDes->target_id = $model->id;

                $modelKeyDes->save();
            }
            if (Yii::$app->request->post('ProductImg')) {
                $modelImg->prod_id = $model->id;
                $modelImg->path = Yii::$app->request->post('ProductImg')['path'];
                $image = UploadedFile::getInstance($modelImg, 'path');

                $modelImg->path = $image->baseName . '.' . $image->extension;

                if ($image->saveAs('../../frontend/web/uploads/' . $modelImg->path)) {
                    $modelImg->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

//            $datas = Yii::$app->request->post('data');
//            print_r($datas); die();
//            $batchData = [];
//            foreach ($datas as $value) {
//                $batchData[] = [
//                    'name' => $value['name'],
//                    'price' => $value['price'],
//                    'description' => $value['description'],
//                    'release_date' => $value['release_date'],
//                    'cat_id' => $value['cat_id'],
//                    'sub_cat_id' => $value['sub_cat_id'],
//                    'brand_id' => $value['brand_id']
//                ];
//            }
//            \Yii::$app->db->createCommand()
//                    ->batchInsert(
//                            'product', [
//                        'name',
//                        'price',
//                        'description',
//                        'release_date',
//                        'cat_id',
//                        'sub_cat_id',
//                        'brand_id'
//                            ], $batchData
//                    )
//                    ->execute();
//            return true;
//        }

        return $this->render('create', [
                    'model' => $model,
                    'modelImg' => $modelImg,
                    'modelKeyDes' => $modelKeyDes,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelImg = $this->findModelImg($id);
        $modelKeyDes = $this->findModelKeyDes($id);
        if (Yii::$app->request->post('Product')) {
            $model->name = Yii::$app->request->post('Product')['name'];
            $model->price = Yii::$app->request->post('Product')['price'];
            $model->description = Yii::$app->request->post('Product')['description'];
            $model->release_date = Yii::$app->request->post('Product')['release_date'];
            $model->cat_id = Yii::$app->request->post('Product')['cat_id'];
            $model->sub_cat_id = Yii::$app->request->post('Product')['sub_cat_id'];
            $model->brand_id = Yii::$app->request->post('Product')['brand_id'];
            $model->save();
            if (Yii::$app->request->post('KeyDes')) {
                $modelKeyDes->keyword = Yii::$app->request->post('KeyDes')['keyword'];
                $modelKeyDes->description = Yii::$app->request->post('KeyDes')['description'];
                $modelKeyDes->ind = 'product';
                $modelKeyDes->target_id = $model->id;

                $modelKeyDes->save();
            }
            if (Yii::$app->request->post('ProductImg')) {
                try {
                    $oldImage = $this->findModelImg($id)->path;
                    $modelImg->prod_id = $model->id;
                    $modelImg->path = Yii::$app->request->post('ProductImg')['path'];
                    $image = UploadedFile::getInstance($modelImg, 'path');

                    $modelImg->path = $image->baseName . '.' . $image->extension;

                    if ($image->saveAs('../../frontend/web/uploads/' . $modelImg->path)) {
                        if ($modelImg->save()) {
                            unlink('../../frontend/web/uploads/' . $oldImage);
                        }
                    }
                } catch (\Exception $e) {  
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
                    'modelImg' => $modelImg,
                    'modelKeyDes' => $modelKeyDes,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $oldImage = $this->findModelImg($id)->path;
        if (empty($oldImage)) {
            $this->findModelKeyDes($id)->delete();
            $this->findModel($id)->delete();
        } else {
            if (unlink('../../frontend/web/uploads/' . $oldImage)) {
                $this->findModelImg($id)->delete();
                $this->findModelKeyDes($id)->delete();
                $this->findModel($id)->delete();
            }
        }

        return $this->redirect(['index']);
    }

    public function actionMainSubCatList($id) {
        $data = Categories::find()
                ->where([
                    'parent' => $id
                ])
                ->all();
        echo '<option value="">Slect Main Sub Categories...</option>';
        foreach ($data as $value) {
            echo '<option value="' . $value->id . '">' . $value->name . '</option>';
        }
    }

    public function actionSubCatList($id) {
        $data = Categories::find()
                ->where([
                    'parent' => $id
                ])
                ->all();
        foreach ($data as $value) {
            echo '<option value="' . $value->id . '">' . $value->name . '</option>';
        }
    }

    public function recursive($data) {
        if ($data['parent'] == 0) {
            echo '<option value="' . $data['id'] . '">Select sub categories...</option>';
        }
        if (!($data['parent'] == 0)) {
            if (sizeof($data) <= 7) {
                echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>';
            }
        }
        for ($i = 0; $i < sizeof($data) - 7; $i++) {
            $this->recursive($data[$i]['node']);
        }
    }

    public function actionBrandList($id) {
        $data = Brand::find()->where(['cat_id' => $id])->all();
        foreach ($data as $value) {
            echo '<option value="' . $value->id . '">' . $value->name . '</option>';
        }
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelImg($id) {
        if (($modelImg = \common\models\ProductImg::findOne(['prod_id' => $id])) !== null) {
            return $modelImg;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelKeyDes($id) {
        if (($modelKeyDes = \common\models\KeyDes::findOne(['ind' => 'product', 'target_id' => $id])) !== null) {
            return $modelKeyDes;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
