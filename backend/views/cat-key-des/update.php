<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KeyDes */

$this->title = 'Update Key Des: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Categories Key Des', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="key-des-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
