<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\KeyDes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="key-des-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'keyword')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?=
    $form->field($model, 'ind')->hiddenInput([
        'maxlength' => true,
        'value' => 'cat',
    ])->label('')
    ?>


    <div class="form-group">
        <label for="cat-id">Categories</label>
        <select id="cat-id" class="form-control prod_cat_id" onchange="
                var id = $(this).val();
                $.post('index.php?r=product/main-sub-cat-list&id=' + id,
                        function (data) {
                            $('select.prod_parent_sub_id').html(data);
                            $('select.prrod-sub-categories').html(
                                        '<option value=' + $('select.prod_cat_id').val() + '>Select Sub Categores...</option>'
                                        );
                                
                            $('select.prod_parent_sub_id').prop('disabled', false);
                            $('select.prrod-sub-categories').prop('disabled', false);
                            $(document).find('.error-sbu').remove();
                        });
                ">
            <option>Select Main Sub Categories...</option>
            <?php
            $cat_list = common\models\Categories::find()->where(['parent' => 0])->all();
            foreach ($cat_list as $ca_list) {
                ?>
                <option value="<?= $ca_list->id ?>"><?= $ca_list->name ?></option>
<?php }
?>
        </select>
    </div>

    <div class="form-group">
        <label for="main-sub-id">Main Sub Categories</label>
        <select id="main-sub-id" class="form-control prod_parent_sub_id" disabled="disabled" onchange="
                var id = $(this).val();
                $.post('index.php?r=product/sub-cat-list&id=' + id,
                        function (data) {
                            if (data.length == 0) {
                                $('select.prrod-sub-categories').html(
                                        '<option value=' + $('select.prod_cat_id').val() + '>Select Sub Categores...</option>'
                                        );
                            } else {
                                $('select.prrod-sub-categories').html(data);
                            }
                            $('select.prrod-sub-categories').prop('disabled', false);
                            $(document).find('.error-sbu').remove();
                        });
                ">
            <option>Select Main Sub Categories...</option>
        </select>
    </div>



<?= $form->field($model, 'target_id')->dropDownList(
            ArrayHelper::map(common\models\Categories::find()->where('parent != 0')->all(), 'id', 'name'), [
        'prompt' => 'Select sub categories....',
        'class' => 'form-control prrod-sub-categories',
        'disabled' => 'true'
            ]
    )->label('Select Sub Categories') ?>

    <div class="form-group">
<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
