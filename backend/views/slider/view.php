<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <div class="form-group">
        <?= Html::img(
            '../../frontend/web/uploads/'.$model->path,
            [
                'width' => '25%',
                'height' => '25%'
            ]
            )?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'path',
            'link',
            'title',
            'description:ntext',
//            'status',
        ],
    ]) ?>

</div>
