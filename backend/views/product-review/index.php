<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-review-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'prod_id',
                'value' => 'prod.name'
            ],
            'name',
            [
                'attribute' => 'review',
                'value' => 'review',
                'contentOptions' => [
                    'style' => 'text-overflow: ellipsis; overflow: hidden;white-space: nowrap;max-width: 100px;'
                    ],
            ],
            [
                'attribute' => 'review_date',
                'value' => 'review_date',
                'filter' => DatePicker::widget([
                    'name' => 'ProductReviewSearch[review_date]',
                    'options' => ['placeholder' => 'Select review date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ])
            ],
            'email:email',
            //'status',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
