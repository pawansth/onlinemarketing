<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductAttribute */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Specification Sub Header', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-attribute-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            [
                'label' => 'Categories Name',
                'value' => $model->cat->name
            ],
            [
                'label' => 'Parent',
                'value' => $model->find('name')->where(['id' => $model->parent])->one()->name
            ],
            [
                'label' => 'Filter',
                'value' => $model->filter ? 'True' : 'False'
            ],
            [
                'label' => 'Range',
                'value' => $model->range ? 'True' : 'False'
            ],
            [
                'label' => 'Row',
                'value' => $model->row
            ],
//            'parent',
//            'status',
        ],
    ]) ?>

</div>
