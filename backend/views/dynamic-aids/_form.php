<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DynamicAids */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dynamic-aids-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'size')->dropDownList([
        '300X600' => '66 X 66',
        '300X250' => '300 X 250',
        '728X90' => '728 X 90',
        '300X600' => '300 X 600',
        '970X250' => '970 X 250',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
