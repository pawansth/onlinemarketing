<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DynamicAidsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dynamic Aids';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dynamic-aids-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dynamic Aids', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div style="overflow-x: auto;">
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'code',
                'value' => 'code',
                'format' => 'ntext',
                'contentOptions' => [
                    'style' => 'text-overflow: ellipsis; overflow: hidden;white-space: nowrap;max-width: 100px;'
                    ],
            ],
//            'code:ntext',
            'size',
//            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>

</div>
