<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use common\models\Categories;

$this->title = 'Add Sub Categories';
$this->params['breadcrumbs'][] = ['label' => 'Categories List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$actionValue = '';
$required = true;
$disabled = true;
if(empty($model->id)){
        $actionValue = Yii::$app->homeUrl.'?r=categories/add-sub';
    } else {
        $actionValue = Yii::$app->homeUrl.'?r=categories/update&id='.$model->id;
//        $required = false;
//        $disabled = false;
    }
?>

<?= Html::csrfMetaTags() ?> 

<?php $form = ActiveForm::begin([
    'action' => $actionValue,
    'method'=> 'POST'
    ])?>

<?= $form->field($model, 'parent')->dropDownList(
 ArrayHelper::map(Categories::find()->where(['parent' => 0])->all(), 'id', 'name'),
        [
            'prompt' => 'SELECT MAIN CATEGORIES',
            'class' => 'form-control main-categories',
            'onchange' => '$.post("index.php?r=categories/sub-cat-list&id="+$(this).val(),'
                . 'function(data) {'
                .'$("select.sub-categories").html(data);'
            . '$("select.sub-categories").prop("disabled", false);'
            .'$(document).find(".error-sbu").remove();'
                . '});',
            'required' => $required
        ]
        )?>

<?= $form->field($model, 'parent')->dropDownList(
 ArrayHelper::map(Categories::find()->where('parent != 0')->all(), 'id', 'name'),
        [
            'prompt' => 'SELECT SUB CATEGORIES',
            'disabled' => $disabled,
            'class' => 'form-control sub-categories'
        ]
        )?>

<div class="container-rows">
    <div class="row form-group row-sub-categories">
        <div class="col-md-10 pull-left">
            <?= $form->field($model, 'name')->textInput([
                'class' => 'form-control sub-name-group'
            ])?>
            <?= $form->field($model, 'link')->textInput([
                'class' => 'form-control sub-link-group'
            ])?>
        </div>
        <?php if(empty($model->id)) {?>
        <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;">
            <?= Html::button('',[
                'class' => 'btn btn-info glyphicon glyphicon-plus  clone-sub-categories',
                'style' => 'border-radius: 50%; font-size: 15px; color: white;'
                . ' width: 25px; height: 25px; margin: 0px; margin-top: 25px;'
                . ' padding: 0px; text-align: center;'
                ]) ?>
        </div>
            <?php
            }
            ?>
    </div>
</div>

<?php if(empty($model->id)) {?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success save-sub-categories-btn']) ?>
    </div>
    <?php
            } else {
            ?>
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-success update-sub-categories-btn']) ?>
    </div>
    <?php 
            }
    ?>

<?php ActiveForm::end(); ?>
