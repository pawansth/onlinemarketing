<?php

$this->title = 'Categories List';
$this->params['breadcrumbs'][] = $this->title;

if(!empty($error)) {
    echo '<div class="jumbotron"><span style="color: red;">'.$error.'</span></div>';
}

echo '<div class="parent" style="border-left: 1px solid black; padding:5px;">';
foreach($model as $value) {
    if(sizeof($value) <= 7) {
        echo '<div class="" style="padding-bottom:5px;">'
    . '<div class="pull-left" style="width:10px !important; height:15px !important; border-bottom: 1px solid black;"></div>'
    . '<div class="">'
                . '<span style="padding-left:3px;">'.$value['name'].'</span>'
                    .'<a href="?r=categories/update&id='.$value['id'].'" class="btn btn-success" style="margin-left: 10px;">'
                    . 'Update'
                    . '</a>'
                .'<a href="?r=categories/delete&id='.$value['id'].'"'
                    . ' class="btn btn-danger" style="margin-left: 10px;"'
                    . ' onclick="if(!confirm(\'Are you sure?\')){return false;}">'
                    . 'Delete'
                    . '</a>'
                . '</div>'
                . '</div>';
    } else {
        echo '<div class="" style="padding-bottom:5px;">'
    . '<div class="pull-left" style="width:10px !important; height:15px !important; border-bottom: 1px solid black;"></div>'
    . '<div class="">'
                . '<span style="padding-left:3px;">'.$value['name'].'</span>'
                    .'<a href="?r=categories/update&id='.$value['id'].'" class="btn btn-success" style="margin-left: 10px;">'
                    . 'Update'
                    . '</a>'
                . '</div>'
                . '</div>';
        recursive($value);
        }
       }
       echo '</div>';
        
 function recursive($data) {
        if((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
            echo '<div style="padding-bottom:5px;">'
            . '<div class="pull-left" style="width:10px !important; height:15px !important; border-bottom: 1px solid black;"></div>'
                    . '<div>'
                    .'<span style="padding-left:3px;">'.$data['name'].'</span>'
                    .'<a href="?r=categories/update&id='.$data['id'].'" class="btn btn-success" style="margin-left: 10px;">'
                    . 'Update'
                    . '</a>'
                    .'<a href="?r=categories/delete&id='.$data['id'].'"'
                    . ' class="btn btn-danger" style="margin-left: 10px;"'
                    . ' onclick="if(!confirm(\'Are you sure?\')){return false;}">'
                    . 'Delete'
                    . '</a>'
                    . '</div>'
                  .'</div>';
        } else {
            if(!($data['parent'] == 0)) {
                echo '<div style="padding-bottom:5px;">'
                . '<div class="pull-left" style="width:10px !important; height:15px !important; border-bottom: 1px solid black;"></div>'
                        . '<div>'
                    .'<span style="padding-left:3px;">'.$data['name'].'</span>'
                    .'<a href="?r=categories/update&id='.$data['id'].'" class="btn btn-success" style="margin-left: 10px;">'
                    . 'Update'
                    . '</a>'
                        . '</div>'
                  .'</div>';
            }
        }
        if(sizeof($data) > 7) {
            echo '<div class="child" style="border-left: 1px solid black; margin-left:10px; padding: 5px;">';
        for($i = 0; $i < sizeof($data)-7; $i++) {
            recursive($data[$i]['node']);  
        }
        echo '</div>';
        }
    }

