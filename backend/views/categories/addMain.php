<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Add Main Categories';
$this->params['breadcrumbs'][] = ['label' => 'Categories List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$actionValue = '';
if(empty($model->id)){
        $actionValue = Yii::$app->homeUrl.'?r=categories/add-main';
    } else {
        $actionValue = Yii::$app->homeUrl.'?r=categories/update&id='.$model->id;
    }
?>

<?php $form = ActiveForm::begin([
    'action' => $actionValue,
    'method'=> 'POST'
    ])?>
<?php 
if(!empty($model->id)) {?>
    <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(FALSE)?>
<?php }
?>

<div class="container-main-categories-rows">
    <div class="row form-group row-main-categories">
        <div class="col-md-10 pull-left">
            <?= $form->field($model, 'name')->textInput([
                'class' => 'form-control main-name-group'
            ])?>
            <?= $form->field($model, 'link')->textInput([
                'class' => 'form-control main-link-group'
            ])?>
        </div>
        <?php if(empty($model->id)) {?>
        <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;">
            <?= Html::button('',[
                'class' => 'btn btn-info glyphicon glyphicon-plus  clone-main-categories',
                'style' => 'border-radius: 50%; font-size: 15px; color: white;'
                . ' width: 25px; height: 25px; margin: 0px; margin-top: 25px;'
                . ' padding: 0px; text-align: center;'
                ]) ?>
        </div>
            <?php
            }
            ?>
    </div>
</div>

<?php if(empty($model->id)) {?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success save-main-categories-btn']) ?>
    </div>
    <?php
            } else {
            ?>
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-success update-main-categories-btn']) ?>
    </div>
    <?php 
            }
    ?>






<?php ActiveForm::end(); ?>
