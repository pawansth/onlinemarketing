<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ImgAids */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Img Aids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="img-aids-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <div class="form-group">
        <?= Html::img(
            '../../frontend/web/uploads/'.$model->path,
            [
                'width' => '25%',
                'height' => '25%'
            ]
            )?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'path',
            'link',
//            'status',
        ],
    ]) ?>

</div>
