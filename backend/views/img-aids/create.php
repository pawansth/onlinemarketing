<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ImgAids */

$this->title = 'Create Img Aids';
$this->params['breadcrumbs'][] = ['label' => 'Img Aids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="img-aids-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
