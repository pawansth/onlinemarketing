<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductAttValue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Specification Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-att-value-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Product Specification Main Header',
                'value' => \common\models\ProductAttribute::find()->where(['id' => $model->prodAtt->parent])->one()->name
            ],
            [
                'label' => 'Product Specification',
                'value' => $model->prodAtt->name
            ],
            'value',
            'unit',
            'detail',
            [
                'label' => 'Product Name',
                'value' => $model->prod->name
            ],
//            'status',
        ],
    ]) ?>

</div>
