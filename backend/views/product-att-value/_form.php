<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductAttValue */
/* @var $form yii\widgets\ActiveForm */
use yii\helpers\ArrayHelper;
use common\models\Product;
use common\models\ProductAttribute;

$actionValue = '';
$required = true;
$disabled = true;

if (empty($model->id)) {
    $actionValue = Yii::$app->homeUrl . '?r=product-att-value/create';
} else {
    $actionValue = Yii::$app->homeUrl . '?r=product-att-value/update&id=' . $model->id;
//        $required = false;
//        $disabled = false;
}
?>

<div class="product-att-value-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'prod_id')->dropDownList(
            ArrayHelper::map(Product::find()->all(), 'id', 'name'), [
        'prompt' => 'Select product name...',
        'class' => 'form-control speci-prod-id',
        'onchange' => '$.post(\'index.php?r=product-att-value/att-header-list&id=\'+$(this).val(),'
        . 'function(data) {'
        . '$("select.speci-header").html(data);'
        . '$("select.speci-header").prop("disabled", false);'
        . '$(document).find(".error-sbu").remove();'
        . '$("select.speci-header").trigger("onchange");'
        . '});',
        'required' => $required
            ]
    )
    ?>

    <?=
    $form->field($model, 'prod_att_id')->dropDownList(
            ArrayHelper::map(ProductAttribute::find()->where('parent = 0')->all(), 'id', 'name'), [
        'prompt' => 'Select product specification header...',
        'class' => 'form-control speci-header',
        'onchange' => '$.post(\'index.php?r=product-att-value/att-list&id=\'+$(this).val(),'
        . 'function(data) {'
        . '$.each($(document).find(\'.speci\'), function(i, elem) {'
        . '$(this).html(data);'
        . '$(this).prop("disabled", false);'
        . '$(document).find(".error-sbu").remove();'
        . '});'
        . '});',
        'required' => $required,
        'disabled' => $disabled
            ]
    )
    ?>



    <div class="speci-rows">
        <div class="row form-group row-speci">
            <div class="col-md-11 pull-left" style="padding: 0px !important; margin: 0px !important;">
                <div class="col-md-3">
                    <?=
                    $form->field($model, 'prod_att_id')->dropDownList(
                            ArrayHelper::map(ProductAttribute::find()->where('parent != 0')->all(), 'id', 'name'), [
                        'prompt' => 'Select product specification header...',
                        'class' => 'form-control speci',
                        'required' => $required,
                        'disabled' => $disabled
                            ]
                    )
                    ?>
                </div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <?=
                    $form->field($model, 'value')->textInput([
                        'maxlength' => true,
                        'class' => 'form-control speci-value',
                        'onfocusout' => '$(this).parent().find(\'.error\').remove();'
                    ])
                    ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'unit')->textInput([
                            'class' => 'form-control speci-unit',
                            'onfocusout' => '$(this).parent().find(\'.error\').remove()',
                            'maxlength' => true
                            ]) ?>
                    </div>
                    <div class="col-md-8">
                        <?= $form->field($model, 'detail')->textInput([
                            'class' => 'form-control speci-detail',
                            'maxlength' => true
                            ]) ?>
                    </div>

                </div>

            </div>
            <?php if (empty($model->id)) { ?>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;">
                    <?=
                    Html::button('', [
                        'class' => 'btn btn-info glyphicon glyphicon-plus  clone-speci',
                        'style' => 'border-radius: 50%; font-size: 15px; color: white;'
                        . ' width: 25px; height: 25px; margin: 0px; margin-top: 25px;'
                        . ' padding: 0px; text-align: center;'
                    ])
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>



    <?php if (empty($model->id)) { ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success save-speci-btn']) ?>
        </div>
        <?php
    } else {
        ?>
        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-success update-speci-btn']) ?>
        </div>
        <?php
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
