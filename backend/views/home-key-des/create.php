<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KeyDes */

$this->title = 'Create Key Des';
$this->params['breadcrumbs'][] = ['label' => 'Home Key Des', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="key-des-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
