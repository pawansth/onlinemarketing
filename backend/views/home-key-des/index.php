<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HomeKeyDesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Key Des';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="key-des-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Key Des', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'keyword:ntext',
            'description:ntext',
//            'ind',
            'target_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
