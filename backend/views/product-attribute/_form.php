<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $model common\models\ProductAttribute */
/* @var $form yii\widgets\ActiveForm */


if (empty($model->id)) {
    $actionValue = Yii::$app->homeUrl . '?r=product-attribute/create';
} else {
    $actionValue = Yii::$app->homeUrl . '?r=product-attribute/update&id=' . $model->id;
//        $required = false;
//        $disabled = false;
}
?>

<div class="product-attribute-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'cat_id')->dropDownList(
            ArrayHelper::map(Categories::find()->where(['parent' => 0])->all(), 'id', 'name'), [
        'prompt' => 'SELECT CATEGORIES NAME',
        'class' => 'form-control prod_att_cat_id'
            ]
    )
    ?>


    <div class="container-prod-att-rows">
        <div class="row form-group row-prod-att">
            <div class="col-md-10 pull-left">
                <?=
                $form->field($model, 'name')->textInput([
                    'class' => 'form-control prod-att-name',
                    'maxlength' => true
                ])
                ?>
            </div>
            <?php if (empty($model->id)) { ?>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;">
                    <?=
                    Html::button('', [
                        'class' => 'btn btn-info glyphicon glyphicon-plus  clone-prod-att',
                        'style' => 'border-radius: 50%; font-size: 15px; color: white;'
                        . ' width: 25px; height: 25px; margin: 0px; margin-top: 25px;'
                        . ' padding: 0px; text-align: center;'
                    ])
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <?php if (empty($model->id)) { ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success save-prod-att-btn']) ?>
        </div>
        <?php
    } else {
        ?>
        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-success update-prod-att-btn']) ?>
        </div>
        <?php
    }
    ?>


    <?php ActiveForm::end(); ?>

</div>
