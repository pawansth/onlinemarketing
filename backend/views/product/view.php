<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <div class="form-group">
        <img src="../../frontend/web/uploads/<?= \common\models\ProductImg::find()
        ->where([
            'prod_id' => $model->id
        ])
        ->one()
        ->path?>" width="200" height="200">
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'price',
            'description',
            'release_date',
            [
                'label' => 'Categories Name',
                'value' => $model->cat->name
            ],
            [
                'label' => 'Sub Categories Name',
                'value' => $model->subCat->name
            ],
            [
                'label' => 'Brand Name',
                'value' => $model->brand->name
            ],
            [
                'label' => 'Meta Keyword',
                'value' => common\models\KeyDes::find()->where([
                    'ind' => 'product',
                    'target_id' => $model->id
                        ])->one()->keyword
            ],
            [
                'label' => 'Meta Description',
                'value' => common\models\KeyDes::find()->where([
                    'ind' => 'product',
                    'target_id' => $model->id
                        ])->one()->description
            ],
//            'status',
        ],
    ]) ?>

</div>
