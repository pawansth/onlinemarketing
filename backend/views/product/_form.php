<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use common\models\Categories;
use common\models\Brand;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
//$actionValue = '';
$required = true;
$disabled = true;
if (!empty($model->id)) {
    $disabled = false;
    $required = false;
}

//if (empty($model->id)) {
//    $actionValue = Yii::$app->homeUrl . '?r=product/create';
//} else {
//    $actionValue = Yii::$app->homeUrl . '?r=product/update&id=' . $model->id;
//        $required = false;
//        $disabled = false;
//}
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'cat_id')->dropDownList(
            ArrayHelper::map(Categories::find()->where(['parent' => 0])->all(), 'id', 'name'), [
        'prompt' => 'Select categories....',
        'class' => 'form-control prod_cat_id',
        'onchange' => 'var id = $(this).val();$.post(\'index.php?r=product/main-sub-cat-list&id=\'+id,'
        . 'function(data) {'
        . '$("select.prod_parent_sub_id").html(data);'
        . '$("select.prod_parent_sub_id").prop("disabled", false);'
        . '$(document).find(".error-sbu").remove();'
        . '});',
        'required' => $required
            ]
    )
    ?>
    <div class="form-group">
        <label for="main-sub-id">Main Sub Categories</label>
        <select id="main-sub-id" class="form-control prod_parent_sub_id" disabled="disabled" onchange="
                var id = $(this).val();
                $.post('index.php?r=product/sub-cat-list&id=' + id,
                        function (data) {
                            $.post('index.php?r=product/brand-list&id=' + $('select.prod_cat_id').val(),
                                    function (data) {
                                        $('select.prod-brand').html(data);
                                        $('select.prod-brand').prop('disabled', false);
                                        $(document).find('.error-sbu').remove();
                                    });
                            if (data.length == 0) {
                                $('select.prrod-sub-categories').html('<option value=' + $('select.prod_cat_id').val() + '>Select Sub Categores...</option>');
                            } else {
                                $('select.prrod-sub-categories').html(data);
                            }
                            $('select.prrod-sub-categories').prop('disabled', false);
                            $(document).find('.error-sbu').remove();
                        });
                ">
            <option>Select Main Sub Categories...</option>
        </select>
    </div>
    <?php
    $subCatList;
    if (!empty($model->id)) {
        $subCatList = Categories::find()->where(
                        'parent = ' . Categories::findOne(['id' => $model->sub_cat_id])->parent
                )->all();
    } else {
        $subCatList = Categories::find()->where('parent != 0')->all();
    }
    ?>
    <?=
    $form->field($model, 'sub_cat_id')->dropDownList(
            ArrayHelper::map($subCatList, 'id', 'name'), [
        'prompt' => 'Select sub categories....',
        'class' => 'form-control prrod-sub-categories',
        'disabled' => $disabled
            ]
    )
    ?>
    <?php
    $brandList;
    if (!empty($model->id)) {
        $brandList = Brand::find()
                ->where([
                    'cat_id' => $model->cat_id
                ])
                ->all();
    } else {
        $brandList = Brand::find()->all();
    }
    ?>
    <?=
    $form->field($model, 'brand_id')->dropDownList(
            ArrayHelper::map($brandList, 'id', 'name'), [
        'prompt' => 'Select brand....',
        'class' => 'form-control prod-brand',
        'disabled' => $disabled,
        'required' => $required
            ]
    )
    ?>


    <div class="container-prod-rows">
        <div style="display: block;">
            <div style="display: block"><label>Product Image</label></div>
            <div style="display: inline-block">
                <img id="prev_img" src="<?php
                if (!empty($modelImg->id)) {
                    echo '../../frontend/web/uploads/' . $modelImg->path;
                }
                ?>" width="300" height="300" alt="Chose Product Image...">
            </div>
            <div style="display: inline-block; padding-left: 10px;">
                <?=
                $form->field($modelImg, 'path')->fileInput([
                    'required' => $required,
                    'onchange' => 'if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#prev_img")
                    .attr("src", e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }'
                ])->label('')
                ?>
            </div>
        </div>
        <div class="row form-group row-prod">
            <div class="form-group col-md-4">
                <?=
                $form->field($model, 'name')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control prod-name',
                    'onfocusout' => '$(this).parent().find(\'.error\').remove()'
                ])
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $form->field($model, 'price')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control prod-price',
                    'onfocusout' => '$(this).parent().find(\'.error\').remove()'
                ])
                ?>
            </div>
            <div class="form-group col-md-4">
                <?= Html::label('Release Date') ?>
                <?=
                DatePicker::widget([
                    'name' => 'Product[release_date]',
                    'id' => 'product-release_date',
                    'value' => date('Y-m-d'),
                    'options' => [
                        'placeholder' => 'Select release date ...',
                        'class' => 'form-control prod-release-date'
                    ],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]);
                ?>
            </div>
        </div>
        <?=
                $form->field($model, 'description')
                ->widget(\yii\redactor\widgets\Redactor::className(), [
                    'options' => [
                        'class' => 'form-control product-description'
                    ],
                    'clientOptions' => [
                        'imageManagerJson' => ['/redactor/upload/image-json'],
                        'imageUpload' => ['/redactor/upload/image'],
                        'fileUpload' => ['/redactor/upload/file'],
                        'plugins' => ['clips', 'fontcolor', 'imagemanager']
                    ],
                ])
        ?>


        <?=
                $form->field($modelKeyDes, 'keyword')
                ->textarea(['rows' => '5'])
                ->label('Meta Keyword');
        ?>

        <?=
                $form->field($modelKeyDes, 'description')
                ->textarea(['rows' => '5'])
                ->label('Meta Description');
        ?>
        <div class="form-group">
            <?= Html::submitButton(empty($model->id) ? 'Save' : 'Update', ['class' => 'btn btn-success']) ?>
        </div>

        <?php //if (empty($model->id)) { ?>
        <!--<div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;">-->
        <?php
//                    Html::button('', [
//                        'class' => 'btn btn-info glyphicon glyphicon-plus  clone-prod',
//                        'style' => 'border-radius: 50%; font-size: 15px; color: white;'
//                        . ' width: 25px; height: 25px; margin: 0px; margin-top: 25px;'
//                        . ' padding: 0px; text-align: center;'
//                    ])
        ?>
        <!--</div>-->
        <?php
//            }
        ?>
    </div>



    <?php // if (empty($model->id)) { ?>
    <!--<div class="form-group">-->
    <?php // Html::submitButton('Save', ['class' => 'btn btn-success save-prod-btn']) ?>
    <!--</div>-->
    <?php
//    } else {
    ?>
    <!--<div class="form-group">-->
    <?php // Html::submitButton('Update', ['class' => 'btn btn-success update-prod-btn']) ?>
    <!--</div>-->
    <?php
//    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
