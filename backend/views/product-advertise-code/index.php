<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductAdvertiseCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Association Link';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-advertise-code-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Association Link', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'prod_id',
                'value' =>'prod.name'
            ],
            'advertiser',
            [
                'attribute' => 'code',
                'value' => 'code',
                'format' => 'ntext',
                'contentOptions' => [
                    'style' => 'text-overflow: ellipsis; overflow: hidden;white-space: nowrap;max-width: 100px;'
                    ],
            ],
//            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
