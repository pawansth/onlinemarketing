<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductAdvertiseCode */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\ArrayHelper;
use common\models\Product;
?>

<div class="product-advertise-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'prod_id')->dropDownList(
 ArrayHelper::map(Product::find()->all(), 'id', 'name'),
            [
                'prompt' => 'Select product name...'
            ]
            ) ?>

    <?= $form->field($model, 'advertiser')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
