<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\ProductImg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-img-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'prod_id')->dropDownList(
 ArrayHelper::map(Product::find()->all(), 'id', 'name'),
            [
                'prompt' => 'Select product ....'
            ]
            ) ?>

    <?= $form->field($model, 'path')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
