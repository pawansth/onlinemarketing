<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductRatingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Ratings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-rating-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Create Product Rating', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'attribute' => 'prod_id',
                'value' => 'prod.name'
            ],
            'rating',
            [
                'attribute' => 'rating_data',
                'value' => 'rating_data',
                'filter' => DatePicker::widget([
                    'name' => 'ProductRatingSearch[rating_data]',
                    'options' => ['placeholder' => 'Select rating date ...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ])
            ],
            'email:email',
            //'status',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
