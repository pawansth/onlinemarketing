<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Product;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\ProductRating */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-rating-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'prod_id')->dropDownList(
            ArrayHelper::map(Product::find()->all(), 'id', 'name'), [
        'prompt' => 'Select Product'
            ]
    )
    ?>

    <?= $form->field($model, 'rating')->textInput() ?>

    <?=
    $form->field($model, 'rating_data')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Enter rating date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
            'autoclose' => true
        ]
    ]);
    ?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
