$(document).ready(function() {
//    $(document).on('click', '.add-sub-categories', function() {
//        $('.sub-categories-container').append('<div class="form-group field-categories-parent">'
//                +'<label class="control-label" for="categories-parent">Main Categories</label>'
//                +'<select id="categories-parent" class="form-control sub-categories" name="Categories[parent]" disabled="">'
//                +'<option value="">SELECT SUB CATEGORIES</option>'
//                +'<option value="2">Mobile</option>'
//                +'</select>'
//
//        +'<div class="help-block"></div>'
//        +'</div>');
//    });
$(document).find('.loader').hide();
$(document).on('click', '.clone-sub-categories', function(e) {
    e.preventDefault();
    var cloneContent = $(this).parent().parent().clone();
    cloneContent.find('.sub-name-group').val('');
    cloneContent.find('.sub-link-group').val('');
    cloneContent.find('.clone-sub-categories').remove();
    cloneContent.append('<div class="col-md-1 pull-left" style="margin: 0px; padding: 0px;">'+
                        '<button type="button" class="btn btn-info glyphicon glyphicon-minus remove-sub-categories" style="border-radius: 50%; font-size: 15px; color: white; width: 25px; height: 25px; margin: 0px; margin-top: 25px; padding: 0px; text-align: center;"></button>'+
                        '</div>');
                cloneContent.appendTo('.container-rows');
});

$(document).on('click', '.remove-sub-categories', function(e) {
    e.preventDefault();
    $(this).parent().parent().remove();
});


$(document).on('click', '.save-sub-categories-btn', function(e) {
    e.preventDefault();
    
    if(! $(document).find('.main-categories').val()) {
        $('<span class="alert-danger error-sbu">This field is required</span>').insertAfter('.main-categories');
        return;
    }
    
    $(document).find('.loader').show();
    $(document).find('.container-div').hide();
    var dataa = [];
    var val = [];
    dataa.push({
        'parent': $('.sub-categories').val()
    });
    $.each($('.row-sub-categories'), function(i, elem) {
        val.push({
            'name': $(this).find('.sub-name-group').val(),
            'link': $(this).find('.sub-link-group').val()
        });
    });
    dataa.push({
        'value': val
    });
    console.log(dataa);
    $.ajax({
                url: 'index.php?r=categories/add-sub',
                method: "POST",
                dataType: 'json',
                data: {'data': dataa},
            }).done(function(data){
                window.location.href = 'index.php?r=categories';
            }).fail(function() {
                $(document).find('.loader').hide();
                $(document).find('.container-div').show();
                if($(document).find('.error-message').length) {
                    return;
                }
                $(document).find('.container-div')
                        .prepend(
                        '<div class="jumbotron">\n\
                        <span class="alert-danger error-message">Some thing went wrong!</span>\n\
                        </div>'
                    );
            });
});




$(document).on('click', '.clone-main-categories', function(e) {
    e.preventDefault();
    var cloneContent = $(this).parent().parent().clone();
    cloneContent.find('.main-name-group').val('');
    cloneContent.find('.main-link-group').val('');
    cloneContent.find('.clone-main-categories').remove();
    cloneContent.append('<div class="col-md-1 pull-left" style="margin: 0px; padding: 0px;">'+
                        '<button type="button" class="btn btn-info glyphicon glyphicon-minus remove-main-categories" style="border-radius: 50%; font-size: 15px; color: white; width: 25px; height: 25px; margin: 0px; margin-top: 25px; padding: 0px; text-align: center;"></button>'+
                        '</div>');
                cloneContent.appendTo('.container-main-categories-rows');
});

$(document).on('click', '.remove-main-categories', function(e) {
    e.preventDefault();
    $(this).parent().parent().remove();
});


$(document).on('click', '.save-main-categories-btn', function(e) {
    e.preventDefault();
    
    $(document).find('.loader').show();
    $(document).find('.container-div').hide();
    var dataa = [];
    
    $.each($('.row-main-categories'), function(i, elem) {
        dataa.push({
            'name': $(this).find('.main-name-group').val(),
            'link': $(this).find('.main-link-group').val()
        });
    });
    
    console.log(dataa);
    $.ajax({
                url: 'index.php?r=categories/add-main',
                method: "POST",
                dataType: 'json',
                data: {'data': dataa},
            }).done(function(data){
                window.location.href = 'index.php?r=categories';
            }).fail(function(e) {
                console.log(e);
                $(document).find('.loader').hide();
                $(document).find('.container-div').show();
                if($(document).find('.error-message').length) {
                    return;
                }
                $(document).find('.container-div')
                        .prepend(
                        '<div class="jumbotron">\n\
                        <span class="alert-danger error-message">Some thing went wrong!</span>\n\
                        </div>'
                    );
            });
});



$(document).on('click', '.clone-prod-att', function(e) {
    e.preventDefault();
    var cloneContent = $(this).parent().parent().clone();
    cloneContent.find('.prod-att-name').val('');
    cloneContent.find('.clone-prod-att').remove();
    cloneContent.append('<div class="col-md-1 pull-left" style="margin: 0px; padding: 0px;">'+
                        '<button type="button" class="btn btn-info glyphicon glyphicon-minus remove-prod-att" style="border-radius: 50%; font-size: 15px; color: white; width: 25px; height: 25px; margin: 0px; margin-top: 25px; padding: 0px; text-align: center;"></button>'+
                        '</div>');
                cloneContent.appendTo('.container-prod-att-rows');
});

$(document).on('click', '.remove-prod-att', function(e) {
    e.preventDefault();
    $(this).parent().parent().remove();
});

$(document).on('change', '.prod_att_cat_id', function(e) {
    e.preventDefault();
    $(document).find('.error-sbu').remove();
});

$(document).on('click', '.save-prod-att-btn', function(e) {
    e.preventDefault();
    var test = 0;
    if(! $(document).find('.prod_att_cat_id').val()) {
        $('<span class="alert-danger error-sbu">This field is required</span>').insertAfter('.prod_att_cat_id');
        return;
    }
    
    $.each($(document).find('.prod-att-name'), function(i, elem) {
        if(! $(this).val()) {
        $('<span class="alert-danger error-sbu">This field is required</span>').insertAfter($(this));
        test = 1;
        return false; 
    }
    });
    
    if(test == 1) {
        return;
    }
    
    $(document).find('.loader').show();
    $(document).find('.container-div').hide();
    var dataa = [];
    
    $.each($('.row-prod-att'), function(i, elem) {
        dataa.push({
            'name'  : $(this).find('.prod-att-name').val(),
            'cat_id': $(document).find('.prod_att_cat_id').val()
        });
    });
    
    console.log(dataa);
    $.ajax({
                url: 'index.php?r=product-attribute/create',
                method: "POST",
                dataType: 'json',
                data: {'data': dataa},
            }).done(function(data){
                window.location.href = 'index.php?r=product-attribute';
            }).fail(function(e) {
                console.log(e);
                $(document).find('.loader').hide();
                $(document).find('.container-div').show();
                if($(document).find('.error-message').length) {
                    return;
                }
                $(document).find('.container-div')
                        .prepend(
                        '<div class="jumbotron">\n\
                        <span class="alert-danger error-message">Some thing went wrong!</span>\n\
                        </div>'
                    );
            });
});



$(document).on('click', '.clone-prod-sub-att', function(e) {
    e.preventDefault();
    var cloneContent = $(this).parent().parent().clone();
    cloneContent.find('.prod-sub-att-name').val('');
    cloneContent.find('.prod-sub-att-row').val(0);
    cloneContent.find('.clone-prod-sub-att').remove();
    cloneContent.append('<div class="col-md-2 pull-left" style="margin: 0px; padding: 0px;">'+
                        '<button type="button" class="btn btn-info glyphicon glyphicon-minus remove-prod-sub-att" style="border-radius: 50%; font-size: 15px; color: white; width: 25px; height: 25px; margin: 0px; margin-top: 25px; padding: 0px; text-align: center;"></button>'+
                        '</div>');
                cloneContent.appendTo('.container-prod-sub-att-rows');
});

$(document).on('click', '.remove-prod-sub-att', function(e) {
    e.preventDefault();
    $(this).parent().parent().remove();
});

$(document).on('change', '.prod_sub_att_cat_id', function(e) {
    e.preventDefault();
    $(document).find('.error-sbu').remove();
    
    $.post("index.php?r=product-sub-attribute/list-parent&id="+$(this).val(),
                function(data) {
                    $(".prod_sub_att_parent").html(data);
                    $("select.prod_sub_att_parent").prop("disabled", false);
                    $(document).find(".error-sbu").remove();
                });
});

$(document).on('change', '.prod_sub_att_parent', function(e) {
    e.preventDefault();
    $(document).find('.error-sbu-parent').remove();
});

$(document).on('click', '.save-prod-sub-att-btn', function(e) {
    e.preventDefault();
    var test = 0;
    if(! $(document).find('.prod_sub_att_cat_id').val()) {
        $('<span class="alert-danger error-sbu">This field is required</span>').insertAfter('.prod_att_cat_id');
        return;
    }
    
    if(! $(document).find('.prod_sub_att_parent').val()) {
        $('<span class="alert-danger error-sbu-parent">This field is required</span>').insertAfter('.prod_att_cat_id');
        return;
    }
    
    $.each($(document).find('.prod-sub-att-name'), function(i, elem) {
        if(! $(this).val()) {
        $('<span class="alert-danger error-sbu-name">This field is required</span>').insertAfter($(this));
        test = 1;
        return false; 
    }
    });
    
    if(test == 1) {
        return;
    }
    
    $(document).find('.loader').show();
    $(document).find('.container-div').hide();
    var dataa = [];
    
    $.each($('.row-prod-sub-att'), function(i, elem) {
        dataa.push({
            'name'  : $(this).find('.prod-sub-att-name').val(),
            'filter': $(this).find('.prod-sub-att-filter').val(),
            'range' : $(this).find('.prod-sub-att-range').val(),
            'row'   : $(this).find('.prod-sub-att-row').val(),
            'cat_id': $(document).find('.prod_sub_att_cat_id').val(),
            'parent': $(document).find('.prod_sub_att_parent').val()
        });
    });

    $.ajax({
                url: 'index.php?r=product-sub-attribute/create',
                method: "POST",
                dataType: 'json',
                data: {'data': dataa},
            }).done(function(data){
                window.location.href = 'index.php?r=product-sub-attribute';
            }).fail(function(e) {
                console.log(e);
                $(document).find('.loader').hide();
                $(document).find('.container-div').show();
                if($(document).find('.error-message').length) {
                    return;
                }
                $(document).find('.container-div')
                        .prepend(
                        '<div class="jumbotron">\n\
                        <span class="alert-danger error-message">Some thing went wrong!</span>\n\
                        </div>'
                    );
            });
});


function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

//$(document).on('click', '.clone-prod', function(e) {
//    e.preventDefault();
//    var cloneContent = $(this).parent().parent().clone();
//    cloneContent.find('.prod-name').val('');
//    cloneContent.find('.prod-price').val('');
//    var randomId = makeid();
//    cloneContent.find('.prod-release-date').attr('id',randomId);
//    cloneContent.find('.clone-prod').remove();
//    cloneContent.append('<div class="col-md-1 pull-left" style="margin: 0px; padding: 0px;">'+
//                        '<button type="button" class="btn btn-info glyphicon glyphicon-minus remove-prod" style="border-radius: 50%; font-size: 15px; color: white; width: 25px; height: 25px; margin: 0px; margin-top: 25px; padding: 0px; text-align: center;"></button>'+
//                        '</div>');
//                cloneContent.appendTo('.container-prod-rows');
//                
//        jQuery.fn.kvDatepicker.dates={};
//        if (jQuery('#'+randomId).data('kvDatepicker')) { jQuery('#'+randomId).kvDatepicker('destroy'); }
//        jQuery('#'+randomId).kvDatepicker(kvDatepicker_d0c65a98);
//        initDPAddon(randomId);            
//        initDPRemove(randomId);
//});
//
//$(document).on('click', '.remove-prod', function(e) {
//    e.preventDefault();
//    $(this).parent().parent().remove();
//});


$(document).on('click', '.save-prod-btn', function(e) {
    e.preventDefault();
    var test = 0;
    if(! $(document).find('.prod_cat_id').val()) {
        $('<span class="alert-danger error-sbu">This field is required</span>').insertAfter('.prod_cat_id');
        return;
    }
    
    
    $.each($(document).find('.row-prod'), function(i, elem) {
        if(! $(this).find('.prod-name').val()) {
            $('<span class="alert-danger error">This field is required</span>').insertAfter($(this).find('.prod-name'));
            test = 1;
            return false; 
        }
        if(! $(this).find('.prod-price').val()) {
            $('<span class="alert-danger error">This field is required</span>').insertAfter($(this).find('.prod-price'));
            test = 1;
            return false; 
        }
    });
    
    if(test == 1) {
        return;
    }
    
    $(document).find('.loader').show();
    $(document).find('.container-div').hide();
    var dataa = [];
    
    $.each($('.row-prod'), function(i, elem) {
        dataa.push({
            'name'  : $(this).find('.prod-name').val(),
            'price': $(this).find('.prod-price').val(),
            'release_date': $(this).find('.prod-release-date').val(),
            'description':  $(this).find('.product-description').val(),
            'cat_id': $(document).find('.prod_cat_id').val(),
            'sub_cat_id': $(document).find('.prrod-sub-categories').val(),
            'brand_id': $(document).find('.prod-brand').val()
        });
    });
console.log(dataa);
    $.ajax({
                url: 'index.php?r=product/create',
                method: "POST",
                dataType: 'json',
                data: {'data': dataa}
            }).done(function(data){
                window.location.href = 'index.php?r=product';
            }).fail(function(e) {
                console.log(e);
                $(document).find('.loader').hide();
                $(document).find('.container-div').show();
                if($(document).find('.error-message').length) {
                    return;
                }
                $(document).find('.container-div')
                        .prepend(
                        '<div class="jumbotron">\n\
                        <span class="alert-danger error-message">Some thing went wrong!</span>\n\
                        </div>'
                    );
            });
});





$(document).on('click', '.clone-speci', function(e) {
    e.preventDefault();
    var cloneContent = $(this).parent().parent().clone();
    cloneContent.find('.speci-value').val('');
    cloneContent.find('.speci-unit').val('');
    cloneContent.find('.speci-detail').val('');
    cloneContent.find('.clone-speci').remove();
    cloneContent.append('<div class="col-md-1 pull-left" style="margin: 0px; padding: 0px;">'+
                        '<button type="button" class="btn btn-info glyphicon glyphicon-minus remove-speci" style="border-radius: 50%; font-size: 15px; color: white; width: 25px; height: 25px; margin: 0px; margin-top: 25px; padding: 0px; text-align: center;"></button>'+
                        '</div>');
                cloneContent.appendTo('.speci-rows');
});

$(document).on('click', '.remove-speci', function(e) {
    e.preventDefault();
    $(this).parent().parent().remove();
});


$(document).on('click', '.save-speci-btn', function(e) {
    e.preventDefault();
    var test = 0;
    if(! $(document).find('.speci-prod-id').val()) {
        $('<span class="alert-danger error-sbu">This field is required</span>').insertAfter('.speci-prod-id');
        return;
    }
    
    
    $.each($(document).find('.row-speci'), function(i, elem) {
        if(! $(this).find('.speci-value').val()) {
            $('<span class="alert-danger error">Value cannot be blank.</span>').insertAfter($(this).find('.speci-value'));
            test = 1;
            return false; 
        }
    });
    
    if(test == 1) {
        return;
    }
    
    $(document).find('.loader').show();
    $(document).find('.container-div').hide();
    var dataa = [];
    
    $.each($('.row-speci'), function(i, elem) {
        dataa.push({
            'prod_att_id'  : $(this).find('.speci').val(),
            'value': $(this).find('.speci-value').val(),
            'unit': $(this).find('.speci-unit').val(),
            'detail': $(this).find('.speci-detail').val(),
            'prod_id': $(document).find('.speci-prod-id').val()
        });
    });
console.log(dataa);
    $.ajax({
                url: 'index.php?r=product-att-value/create',
                method: "POST",
                dataType: 'json',
                data: {'data': dataa}
            }).done(function(data){
                window.location.href = 'index.php?r=product-att-value';
            }).fail(function(e) {
                console.log(e);
                $(document).find('.loader').hide();
                $(document).find('.container-div').show();
                if($(document).find('.error-message').length) {
                    return;
                }
                $(document).find('.container-div')
                        .prepend(
                        '<div class="jumbotron">\n\
                        <span class="alert-danger error-message">Some thing went wrong!</span>\n\
                        </div>'
                    );
            });
});



});


