<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\models;

/**
 * Description of ProductSubAttributeSearch
 *
 * @author pawan
 */
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProductAttribute;

class ProductSubAttributeSearch extends ProductAttribute {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'filter', 'range', 'row', 'parent', 'status'], 'integer'],
            [['name', 'cat_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductAttribute::find()
                ->joinWith('cat')
                ->andFilterWhere(['!=', 'product_attribute.parent', 0]);

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

// grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'filter' => $this->filter,
            'range' => $this->range,
            'row' => $this->row,
            'parent' => $this->parent,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'product_attribute.name', $this->name]);
        $query->andFilterWhere(['like', 'categories.name', $this->cat_id]);

        return $dataProvider;
    }

}
