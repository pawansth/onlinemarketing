<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_review".
 *
 * @property int $id
 * @property int $prod_id
 * @property string $review
 * @property string $review_date
 * @property string $email
 * @property int $status
 *
 * @property Product $prod
 */
class ProductReview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prod_id', 'review', 'review_date', 'email'], 'required'],
            [['prod_id', 'status'], 'integer'],
            [['review_date'], 'safe'],
            [['review'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
            [['prod_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['prod_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Prod ID',
            'review' => 'Review',
            'review_date' => 'Review Date',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(Product::className(), ['id' => 'prod_id']);
    }
}
