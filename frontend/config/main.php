<?php

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'onlinemarketing565342@gmail.com',
                'password' => '/*password-1-online-2-marketing-3*/',
                'port' => '587',
                'encryption' => 'tls'
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<alias:contact|about|privacy-policy>' => 'site/<alias>',
                'product-list/<id:.*>' => 'product/product-list',
                'product-detail/<id:.*>/<brandId:.*>' => 'product/product-detail',
                'product-review/<id:.*>' => 'product/product-review',
                'product-rate/<id:.*>' => 'product/product-rate',
                'product-list-search' => 'product/product-list-search',
                'compare-product-search' => 'product/compare-product-search',
                'product-search' => 'product/product-search',
                'product-compare' => 'product/product-compare',
                'upcoming/<id:.*>' => 'product/upcoming',
            ],
        ],
    ],
    'params' => $params,
];
