<?php
$this->registerCss(
        ".compare-aid {"
        . "width: 100%;"
        . "margin-top: 20px;"
        . "padding-bottom: 20px;"
        . "margin-left: 20px;"
        . "}"
        . "@media only screen and (max-width: 991px) {"
        . ".compare-aid {"
        . "margin-left: 0;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 969px) {"
        . ".compare-aid {"
        . "display: none"
        . "}"
        . "}"
        . "@media only screen and (max-width: 767px) {"
        . ".compare-aid {"
        . "display: none"
        . "}"
        . "}"
        . "@media only screen and (max-width: 480px) {"
        . ".compare-aid {"
        . "display: none"
        . "}"
        . "}"
);

$cat_id = common\models\Product::find()
                ->select('cat_id')
                ->where([
                    'id' => $products['product-0']
                ])
                ->one()
        ->cat_id;

$this->registerCss(
        ".checked {"
        . "color: orange;"
        . "}"
);
$title = '';
foreach ($products as $key => $value) {
    if (!empty($value)) {
        if ($key === 'product-0') {
            $title = $title . common\models\Product::find()
                            ->where(['id' => $value])->one()->name;
        } else {
            $title = $title . ' VS ' . common\models\Product::find()
                            ->where(['id' => $value])->one()->name;
        }
    }
}
$this->title = $title .'| allgadgetreview.com';
$parentTitle = \common\models\Categories::find()
                ->where([
                    'id' => common\models\Product::find()
                    ->where(['id' => $products['product-0']])->one()->sub_cat_id
                ])
                ->one()
        ->name;
$this->params['breadcrumbs'][] = [
    'label' => $parentTitle,
    'url' => [
        'product-list/' . common\models\Product::find()
                ->where(['id' => $products['product-0']])->one()->sub_cat_id
    ]
];
$this->params['breadcrumbs'][] = $title;
?>

<div class="compare_main_div" style="background: #FFFFFF; margin-left: 5%; margin-right: 5%; margin-top: 1.5%;">
    <div style="width: 100%;">
        <table class="table-bordered" style="width: 100%;">
            <tr style="width: 100%; height: 25vh;">
                <td style="width: 20%;"></td>
<?php foreach ($products as $key => $value) {
    ?>
                    <td class="<?= $key ?>_img" style="width: 20%;">
                    <?php
                    if (empty($value))
                        continue;
                    ?>
                        <div>
                            <span style="padding: 10px; font-weight: bold;">
                                <?php
                                $prodName = common\models\Product::find()
                                        ->select('name')
                                        ->where([
                                            'id' => $value
                                        ])
                                        ->one();
                                if (!empty($prodName))
                                    echo $prodName->name;
                                ?>
                            </span>
                        </div>
                        <div style="height: 85%; width: 100%; display: block;">
                            <img src="<?php
                            $imgUrl = \common\models\ProductImg::find()
                                    ->select('path')
                                    ->where([
                                        'prod_id' => $value
                                    ])
                                    ->one();
                            if (!empty($imgUrl))
                                echo 'uploads/' . $imgUrl->path;
                                ?>" style="width: 40%; height: 100%; margin: 5%;">
                        </div>
                        <div style="width: 100%; display: block; margin-left: 5%;">
                            <?php
                            $rating = common\models\ProductRating::find()
                                    ->select('SUM(rating) / COUNT(rating) as rating')
                                    ->where([
                                        'prod_id' => $value
                                    ])
                                    ->groupBy('prod_id')
                                    ->one();
                            if (!empty($rating)) {
                                ?>
                                <?php
                                if ($rating->rating == 1) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                } elseif ($rating->rating > 1 && $rating->rating < 2) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-half-full checked"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                } elseif ($rating->rating == 2) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                } elseif ($rating->rating > 2 && $rating->rating < 3) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-half-full checked"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                } elseif ($rating->rating == 3) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                } elseif ($rating->rating > 3 && $rating->rating < 4) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-half-full checked"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                } elseif ($rating->rating == 4) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                } elseif (($rating->rating > 4) && ($rating->rating < 5)) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-half-full checked"></span>
                                    <?php
                                } elseif ($rating->rating == 5) {
                                    ?>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <?php
                                } else {
                                    ?>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <?php
                                }
                                ?>
                                <?php
                            } else {
                                ?>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <?php
                            }
                            ?>
                        </div>
                    </td>
                <?php }
                ?>
            </tr>
        </table>
    </div>
    
        <?php
        
        $add970 = \common\models\DynamicAids::find()
                    ->where([
                        'size' => '970X250'
                    ])
                    ->all();
        
        $headCounter = 0;
        $spcMainTitle = common\models\ProductAttribute::find()
                ->where([
                    'cat_id' => $cat_id,
                    'parent' => 0
                ])
                ->all();
        if (!empty($spcMainTitle)) {
            foreach ($spcMainTitle as $key => $value) {

                $attList = \common\models\ProductAttribute::find()
                        ->where(
                                'cat_id = ' . $cat_id . ' AND parent = '.$value->id
                        )
                        ->all();

                if ($headCounter == 4) {
                    foreach ($add970 as $ad970) {
                        ?>
                        <div class="under_review_add" style="width: 100%; padding-top: 10px; padding-bottom: 10px;">
                            <div style="width: 970px;
                                 height: 250px; margin-left: auto;
                                 margin-right: auto; padding-top: 10px;
                                 padding-bottom: 10px;">
                                 <?= $ad970->code ?>
                            </div>
                        </div>
                        <?php
                        break;
                    }
                    $headCounter = 0;
                }
                ?>
                    <div style="
                         font-size: 18px;
                         font-weight: bold;
                         color: #AAA;
                         padding-left: 20px;
                         padding-top: 15px;
                         border-bottom: 1px solid #E6E6E6;
                         "><?= $value->name ?></div>
                    
                    <table class="table-bordered" style="width: 100%;">
                       <?php if(!empty($attList)) { foreach($attList as $key => $value) {
    ?>
            <tr style="width: 100%; height: 20vh;">
                <td style="width: 20%; text-align: center;"><?= $value->name ?></td>
    <?php foreach($products as $prodKey => $prodValue) {?>
                <td class="<?= $prodKey?>_<?= $key?>" style="width: 20%; text-align: center;">
                    <div style="width: 100%;">
                        <div style="width: 85%"></div>
                    </div>
    <?php
        if(empty($prodValue))            continue;
                $attVal = common\models\ProductAttValue::find()
                        ->where(
                                'prod_att_id = '. $value->id .' AND prod_id = '. $prodValue
                                )
                        ->one();
                if(!empty($attVal)) echo $attVal->value .' '. $attVal->unit .' '. $attVal->detail;
    ?>
                </td>
    <?php }
    ?>
            </tr>
                       <?php } }
    $headCounter++;
    ?>
                    </table>
                    
          <?php  }
        }
        ?>
        

    <div class="compare-aid">
        <?php
        $compareAid = common\models\DynamicAids::find()
                ->where([
                    'size' => '970X250'
                ])
                ->all();
        foreach ($compareAid as $comAid) {
            echo $comAid->code;
            break;
        }
        ?>
    </div>

</div>

