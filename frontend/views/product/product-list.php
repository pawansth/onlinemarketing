<?php

use yii\helpers\Html;

Html::csrfMetaTags();
$title = \common\models\Categories::find()
                ->where([
                    'id' => $sub_cat_id
                ])
                ->one()
        ->name;
$this->title = $title . '|allgadgetreview.com';

$keyDes = \common\models\KeyDes::findOne([
    'ind' => 'cat',
    'target_id' => $sub_cat_id
    ]);

$this->des = $keyDes->description;
$this->key = $keyDes->keyword;

$this->params['breadcrumbs'][] = $title;



$this->registerCss(
        ".live-search-result:hover {"
        . "background: #EE684B;"
        . "cursor: pointer;"
        . "}"
        . ".product-list-content {"
        . "background: #FFFFFF;"
        . "padding: 20px;"
        . "min-height: 100vh;"
        . "}"
        . ".jumbotron-holder {"
        . "background: #FFFFFF;"
        . "padding: 20px;"
        . "height: auto;"
        . "}"
        . ".product-main-container {"
        . "width: 78%;"
        . "float: left;"
        . "padding-left: 20px;"
        . "min-height: 100vh;"
        . "}"
        . ".filters-list {"
        . "width: 22%;"
        . "float: left;"
        . "background: #FFFFFF;"
        . "}"
        . ".checked {"
        . "color: orange;"
        . "}"
        . ".product-item {"
        . "width: 24%; height: 64%; display: inline-block; solid; margin: 3px;"
        . "}"
        . ".product-item:hover {"
        . "box-shadow: 1px 1px 2px 2px #888888;"
        . "}"
        . ".fixed-aid {"
        . "width: 100%;"
        . "padding: 5px;"
        . "position: fixed;"
        . "top: 0;"
        . "left: 0;"
        . "z-index: 2;"
        . "display: none;"
        . "}"
        . "@media only screen and (max-width: 991px) {"
        . ".product-list-content {"
        . "padding: 30px;"
        . "}"
        . ".product-main-container {"
        . "width: 100%;"
        . "padding-left: 0;"
        . "}"
        . ".filters-list {"
        . "width: 100%;"
        . "}"
        . ".product-item {"
        . "display: inline-block;"
        . "width: 30%;"
        . "height: 64%;"
        . "}"
        . "}"
        . ".product-name {"
        . "width: 50%;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "overflow: hidden;"
        . "white-space: nowrap;"
        . "text-overflow: ellipsis;"
        . "}"
        . ".compare-div {"
        . "width: 100%;"
        . "margin-top: 5px;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "padding-top: 3px;"
        . "border-top: 1px solid #969C9D;"
        . "display: none;"
        . "}"
        . ".compare-sub-div {"
        . "width: 50%;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "}"
        . ".compare-label:hover {"
        . "cursor: pointer;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 767px) {"
        . ".product-list-content {"
        . "padding: 30px;"
        . "}"
        . ".product-main-container {"
        . "width: 100%;"
        . "padding-left: 0px;"
        . "}"
        . ".filters-list {"
        . "width: 100%;"
        . "}"
        . ".product-item {"
        . "display: inline-block;"
        . "width: 48%;"
        . "height: 64%;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 480px) {"
        . ".product-list-content {"
        . "padding: 30px;"
        . "}"
        . ".product-main-container {"
        . "width: 100%;"
        . "padding-left: 0px;"
        . "}"
        . ".filters-list {"
        . "width: 100%;"
        . "}"
        . ".product-item {"
        . "display: block;"
        . "width: 100%;"
        . "height: 64%;"
        . "}"
        . "}"
);

$this->registerJs(
        "$(document).on('scroll', function(e) {"
        . "var elem = $(e.currentTarget);"
        . "if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {"
        . "return;"
        . "}"
        . "if ((elem.scrollTop() + 100) >= $(document).find('.filters-list-content').outerHeight()) {"
        . "$(document).find('.fixed-aid').css('display', 'block');"
        . "$(document).find('.filters-list-content').css('display', 'none');"
        . "$(document).find('.product-main-container').css('margin-left', '22%');"
        . "}"
        . "if ((elem.scrollTop() + 100) < $(document).find('.filters-list-content').outerHeight()) {"
        . "$(document).find('.fixed-aid').css('display', 'none');"
        . "$(document).find('.filters-list-content').css('display', 'block');"
        . "$(document).find('.product-main-container').css('margin-left', '0');"
        . "}"
        . "})"
);
?>
<?php if (empty($products)) { ?>
    <div class="jumbotron-holder">
        <div class="jumbotron" style="background: #F0795F; color: white;">
            No product found....
        </div>
    </div>

<?php } else {
    ?>
    <div id="product-container">
        <div class="filters-list">
            <div class="filters-list-content">
                <?= $filters ?>
            </div>
            <div class="fixed-aid">
                <?php
                $dyAi = common\models\DynamicAids::find()
                        ->where([
                            'size' => '300X600'
                        ])
                        ->all();
                foreach ($dyAi as $d) {
                    echo $d->code;
                    break;
                }
                ?>
            </div>
        </div>

        <div class="product-main-container">
            <input type="text" hidden="true" id="cat_id" value="<?= $products[0]['cat_id'] ?>"/>
            <input type="text" hidden="true" id="sub_cat_id" value="<?= $sub_cat_id ?>"/>
            <div class="product-list-content">
                <?php // print_r($products);  ?>
                <?php foreach ($products as $ink => $product) { ?>
                    <a href="/product-detail/<?= $product['id'] ?>/<?= $product['brand_id'] ?>">
                        <div class="product-item" prod_id="<?= $product['id'] ?>">
                            <div class="product-img" style="
                                 width: 100%; height: 75%; padding: 10px; position: relative;">
                                 <?php
                                 $date_diff = intval(date_diff(date_create($product['release_date']), date_create(date('Y-m-d')))->format('%R%a'));
                                 if ($date_diff > 0 && $date_diff < 90) {
                                     ?>
                                    <div class="product-label" style="
                                         z-index: 1; position: absolute; background: #ff1744; color: white; font-weight: bold; padding: 5px;">
                                        <span class="sale">New</span>
                                    </div>
                                <?php } elseif ($date_diff < 0) { ?>
                                    <div class="product-label" style="
                                         z-index: 1; position: absolute; background: #ff1744; color: white; font-weight: bold; padding: 5px;">
                                        <span class="sale">Upcoming</span>
                                    </div>
                                <?php } ?>  
                                <img src="uploads/<?= $product['path'] ?>" style="
                                     width: 98%;
                                     height: 100%;
                                     display: block;
                                     margin-left: auto;
                                     margin-right: auto;
                                     "/>
                            </div>
                            <div class="product-detail" style="width: 100%; height: 30%">
                                <div class="product-name">
                                    <span ><?= $product['name']; ?></span><br>
                                    <span><span class="fa fa-dollar"></span> <?= $product['price'] ?></span>
                                </div>
                                <div class="product-star" style="width: 50%; margin-left: auto; margin-right: auto;">
                                    <?php
                                    if ($product['rating'] == 1) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <?php
                                    } elseif ($product['rating'] > 1 && $product['rating'] < 2) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-half-full checked"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <?php
                                    } elseif ($product['rating'] == 2) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <?php
                                    } elseif ($product['rating'] > 2 && $product['rating'] < 3) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-half-full checked"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <?php
                                    } elseif ($product['rating'] == 3) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <?php
                                    } elseif ($product['rating'] > 3 && $product['rating'] < 4) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-half-full checked"></span>
                                        <span class="fa fa-star"></span>
                                        <?php
                                    } elseif ($product['rating'] == 4) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-o"></span>
                                        <?php
                                    } elseif (($product['rating'] > 4) && ($product['rating'] < 5)) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star-half-full checked"></span>
                                        <?php
                                    } elseif ($product['rating'] == 5) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>
                                        <?php
                                    }
                                    ?>
                                </div>
<?php $this->registerCss(
        ".compare-sub-div>input:hover {"
        . "cursor: pointer;"
        . "}"
        . ".compare-sub-div>label:hover {"
        . "cursor: pointer;"
        . "}"
        );?>
                                <div class="compare-div">
                                    <div class="compare-sub-div">
                                            <input type="checkbox" id="<?= $product['name']; ?>_<?= $ink?>"
                                                   class="compare" prod_id="<?= $product['id'] ?>" prod_img="uploads/<?= $product['path'] ?>" prod_name="<?= $product['name'] ?>">
                                            <label for="<?= $product['name']; ?>_<?= $ink?>">
                                             Compare
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a> 
                <?php }
                ?>
            </div>
        </div>

    </div>
<?php } ?>


<?php
$this->registerCss(
        ".show_compare_main {"
        . "position: fixed;"
        . "bottom: 0;"
        . "width: 100%;"
        . "height: 35vh;"
        . "display: none;"
        . "z-index: 1;"
        . "}"
        . ".show_compare_sub_main {"
        . "width: 65%;"
        . "height: 100%;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "padding-bottom: 1%;"
        . "background: #FFFFFF;"
        . "border: 1px solid;"
        . "}"
        . ".show_compare_item {"
        . "width: 20%;"
        . "height: 100%;"
        . "display: block;"
        . "margin: 5px;"
        . "display: inline-block;"
        . "border: 1px solid;"
        . "}"
        . ".close_button {"
        . "float: right;"
        . "z-index: 2;"
        . "top: 0;"
        . "right: 0;"
        . "height: 20px;"
        . "width: 20px;"
        . "background: #848D8E;"
        . "color: white;"
        . "text-align: center;"
        . "}"
        . ".close_button:hover {"
        . "background: #283233;"
        . "cursor: pointer;"
        . "}"
        . ".show_compare_item_img {"
        . "margin: 3px;"
        . "width: 100%;"
        . "height: 85%;"
        . "padding: 5px;"
        . "}"
        . ".show_compare_item_img_item {"
        . "width: 100%;"
        . "height: 100%;"
        . "}"
        . ".show_compare_item_name {"
        . "width: 50%;"
        . "height: 10%;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "padding: 2px;"
        . "overflow: hidden;"
        . "white-space: nowrap;"
        . "text-overflow: ellipsis;"
        . "}"
);
?>

<div class="show_compare_main">
    <div class="show_compare_sub_main">

    </div>
</div>