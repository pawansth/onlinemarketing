<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Product;

use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model common\models\ProductRating */
/* @var $form yii\widgets\ActiveForm */

$title = Product::find()->where(['id' => $id])->one()->name;
$this->title = $title . '|allgadgetreview.com';
?>

<div class="product-rating-form" style="
     margin-left: 25%;
     margin-right: 25%;
     margin-top: 30px;
     background: #FFFFFF;
     padding: 20px;
     ">
    
    <div style="border-bottom: 1px solid #C6B7B3; font-size: 18px; font-weight: bold;">
        <?= Product::find()->where(['id' => $id])->one()->name ?>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'prod_id')->hiddenInput([
        'value' => $id
    ])->label('')
    ?>

    <div class="form-group">
        <label>Rating</label>
        <span class="rating">
            <input id="rating5" type="radio" name="ProductRating[rating]" value="5">
            <label for="rating5">5</label>
            <input id="rating4" type="radio" name="ProductRating[rating]" value="4">
            <label for="rating4">4</label>
            <input id="rating3" type="radio" name="ProductRating[rating]" value="3">
            <label for="rating3">3</label>
            <input id="rating2" type="radio" name="ProductRating[rating]" value="2">
            <label for="rating2">2</label>
            <input id="rating1" type="radio" name="ProductRating[rating]" value="1" checked="true">
            <label for="rating1">1</label>
        </span>
    </div>
    <?php
    $this->registerCss(".rating {
  overflow: hidden;
  vertical-align: bottom;
  display: inline-block;
  width: auto;
  height: 30px;
}

.rating > input {
  opacity: 0;
  margin-right: -100%;
}

.rating > label {
  position: relative;
  display: block;
  float: right;
  background: url('/css/star-off-big.png');
  background-size: 30px 30px;
}

.rating > label:before {
  display: block;
  opacity: 0;
  content: '';
  width: 30px;
  height: 30px;
  background: url('/css/star-on-big.png');
  background-size: 30px 30px;
  transition: opacity 0.2s linear;
}

.rating > label:hover:before,  .rating > label:hover ~ label:before,  .rating:not(:hover) > :checked ~ label:before { opacity: 1; }");
    ?>



    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

