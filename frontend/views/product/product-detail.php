<?php
$this->registerCss(
        ".container {"
        . "min-height: 100vh;"
        . "margin-top: 20px;"
        . "background: #FFFFFF;"
        . "}"
        . ".product_image {"
        . "width: 40%;"
        . "height: 50%;"
        . "margin-top: 20px;"
        . "margin-bottom: 20px;"
        . "}"
        . ".adv-main-div {"
        . "margin-top: 20px;"
        . "margin-bottom: 20px;"
        . "}"
        . ".checked {"
        . "color: orange;"
        . "}"
        . ".review_rating {"
        . "width: 100%;
                     border-top: 1px solid #DADADA;
                     border-bottom: 1px solid #DADADA;
                     padding: 0;
                     margin: 0;"
        . "}"
        . ".overall_rating {"
        . "width: 32%;
                         min-height: 100%;
                         text-align: center;"
        . "}"
        . ".graph {"
        . "width: 32%;
                         min-height: 100%;
                         border-left: 1px solid #DADADA;
                         border-right: 1px solid #DADADA;
                         padding-top: 33px;
                         color: #B8B1B0;"
        . "}"
        . ".btns {"
        . "width: 32%;
                         min-height: 100%;
                         text-align: center;"
        . "}"
        . ".graph-row {"
        . "width: 100%; height: 25px;"
        . "}"
        . "@media only screen and (max-width: 991px) {"
        . ".review_rating {"
        . "border-top: none;"
        . "border-bottom: none;"
        . "}"
        . ".overall_rating {"
        . "width: 50%;"
        . "float: left;"
        . "}"
        . ".graph {"
        . "width: 50%;"
        . "float: left;"
        . "border-left: none;"
        . "border-right: none;"
        . "}"
        . ".btns {"
        . "width: 100%;"
        . "}"
        . ".product_image {"
        . "width: 50%;"
        . "height: 50%;"
        . "}"
        . ".adv-main-div {"
        . "width: 50%;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 972) {"
        . ".under_review_add {"
        . "display: none;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 767px) {"
        . ".review_rating {"
        . "border-top: none;"
        . "border-bottom: none;"
        . "}"
        . ".under_review_add {"
        . "display: none;"
        . "}"
        . ".overall_rating {"
        . "width: 100%;"
        . "}"
        . ".graph {"
        . "width: 100%;"
        . "border-left: none;"
        . "border-right: none;"
        . "}"
        . ".btns {"
        . "width: 100%;"
        . "}"
        . ".product_image {"
        . "width: 100%;"
        . "height: 50%;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 480px) {"
        . ".review_rating {"
        . "border-top: none;"
        . "border-bottom: none;"
        . "}"
        . ".under_review_add {"
        . "display: none;"
        . "}"
        . ".overall_rating {"
        . "width: 100%;"
        . "}"
        . ".graph {"
        . "width: 100%;"
        . "border-left: none;"
        . "border-right: none;"
        . "}"
        . ".btns {"
        . "width: 100%;"
        . "}"
        . ".product_image {"
        . "width: 100%;"
        . "height: 50%;"
        . "}"
        . "}"
);


$this->title = $productDetail[0]['name'] . '|allgadgetreview.com';

$keyDes = \common\models\KeyDes::findOne([
    'ind' => 'prod',
    'target_id' => $$productDetail[0]['id']
    ]);

$this->des = $keyDes->description;
$this->key = $keyDes->keyword;

$parentTitle = \common\models\Categories::find()
                ->where([
                    'id' => $productDetail[0]['sub_cat_id']
                ])
                ->one()
        ->name;
$this->params['breadcrumbs'][] = [
    'label' => $parentTitle,
    'url' => [
        'product-list/' . $productDetail[0]['sub_cat_id']
    ]
];
$this->params['breadcrumbs'][] = $productDetail[0]['name'];
?>

<div class="container" style="background: #FFFFFF;">
    <div style="padding: 10px; border-bottom: 1px solid #DADADA;">
        <h2 style="
            font-size: 18px;
            "><?= $productDetail[0]['name'] ?> Full Specification</h2>
    </div>
    <div class="col-md-5 product_image pull-left">
        <a href="uploads/<?= $productDetail[0]['path'] ?>" data-spzoom>
            <img class="product-image" src="uploads/<?= $productDetail[0]['path'] ?>" style="
                 width: 95%;
                 height: 100%;
                 ">
        </a>
    </div>
    
    <div class="col-md-7 adv-main-div pull-left">
        <?php
        $add = \common\models\DynamicAids::find()
                ->where([
                    'size' => '300X250'
                ])
                ->all();
        foreach ($add as $ad) {
            ?>
            <div style="width: 100%;">
                <div style="margin-bottom: 10px;
                     width: 300px; height: 250px; margin-left: auto; margin-right: auto;">
                     <?= $ad->code ?>
                </div>
            </div>
            <?php
            break;
        }
        ?>
        <h2>Go To Store</h2>
        <?php foreach ($prodAdvCode as $code) { ?>
            <div class="<?= $code['advertiser'] ?>" style="margin: 10px;">
                <?= $code['code'] ?>
            </div>
        <?php }
        ?>
    </div>

    <?php if (strlen($productDetail[0]['description']) != 0) { ?>
        <div class="section">
            <div class="section-title" style="
                 width: 100%;
                 padding-left: 10px;
                 padding-bottom: 5px;
                 font-size: 18px;
                 font-weight: bold;">
                Description
            </div>
            <div style="
                 text-justify: auto;
                 word-wrap: break-word;
                 ">
                     <?= $productDetail[0]['description'] ?>
            </div>
        </div>
    <?php }
    ?>


    <div class="section">
        <!-- container -->
        <div class="container-fluid">

            <div class="row review_rating">
                <div class="col-md-4 overall_rating">
                    <div style="margin-top: 30px; color: #645C5B; font-size: 16px; font-weight: bold; color: #A39F9E;">
                        <span>OVERALL RATING</span>
                    </div>
                    <?php
                    $rate = Yii::$app->db->createCommand('SELECT SUM(rating)/COUNT(rating) AS rating, COUNT(rating) AS total FROM product_rating WHERE prod_id= ' . $productDetail[0]['id'] . ' GROUP BY prod_id')
                            ->queryAll();
                    ?>
                    <div>
                        <span style="
                              font-size: 50px;
                              font-weight: bold;
                              margin: 0;
                              padding: 0;
                              ">
                                  <?= !empty($rate) ? number_format((float) $rate[0]['rating'], 2, '.', '') : 0; ?>
                        </span>
                        <span style="
                              font-size: 20px;
                              color: #A39F9E;
                              margin: 0;
                              padding: 0;
                              ">
                            /5
                        </span>
                        <span style="
                              display: block;
                              font-size: 14px;
                              font-weight: bold;
                              color: #A39F9E;
                              ">
                            BASED ON <?= !empty($rate) ? $rate[0]['total'] : 0; ?> RATING(S)
                        </span>
                    </div>
                </div>
                <div class="col-md-4 graph">
                    <?php
                    $totalRateCount = Yii::$app->db->createCommand('SELECT'
                                    . ' COUNT(rating) AS total FROM product_rating'
                                    . ' WHERE prod_id= ' . $productDetail[0]['id']
                                    . ' GROUP BY prod_id')
                            ->queryAll();
                    $rate5 = Yii::$app->db->createCommand('SELECT'
                                    . ' COUNT(rating) AS total FROM product_rating'
                                    . ' WHERE prod_id= ' . $productDetail[0]['id']
                                    . ' AND rating = 5 GROUP BY prod_id')
                            ->queryAll();
                    ?>
                    <div class="graph-row">
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="font-size: 12px;">5 stars</span>
                        </div>
                        <div style="display: inline-block; width: 60%; float: left;">
                            <div style="
                                 border: 1px solid #D6785E;
                                 width: 100%;
                                 height: 20px;
                                 ">
                                <div style="
                                     height: 100%;
                                     width: <?= !empty($rate5) ? intval(($rate5[0]['total'] / $totalRateCount[0]['total']) * 100) : 0; ?>%;
                                     background: red;
                                     "></div>
                            </div>
                        </div>
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="padding-left: 5px; font-size: 10px">
                                <?= !empty($rate5) ? $rate5[0]['total'] : 0; ?>
                            </span>
                        </div>

                    </div>

                    <?php
                    $rate4 = Yii::$app->db->createCommand('SELECT'
                                    . ' COUNT(rating) AS total FROM product_rating'
                                    . ' WHERE prod_id= ' . $productDetail[0]['id']
                                    . ' AND rating = 4 GROUP BY prod_id')
                            ->queryAll();
                    ?>
                    <div class="graph-row">
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="font-size: 12px;">4 stars</span>
                        </div>
                        <div style="display: inline-block; width: 60%; float: left;">
                            <div style="
                                 border: 1px solid #D6785E;
                                 width: 100%;
                                 height: 20px;
                                 ">
                                <div style="
                                     height: 100%;
                                     width: <?= !empty($rate4) ? intval(($rate4[0]['total'] / $totalRateCount[0]['total']) * 100) : 0; ?>%;
                                     background: red;
                                     "></div>
                            </div>
                        </div>
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="padding-left: 5px; font-size: 10px;"><?= !empty($rate4) ? $rate4[0]['total'] : 0; ?></span>
                        </div>

                    </div>

                    <?php
                    $rate3 = Yii::$app->db->createCommand('SELECT'
                                    . ' COUNT(rating) AS total FROM product_rating'
                                    . ' WHERE prod_id= ' . $productDetail[0]['id']
                                    . ' AND rating = 3 GROUP BY prod_id')
                            ->queryAll();
                    ?>
                    <div class="graph-row">
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="font-size: 12px;">3 stars</span>
                        </div>
                        <div style="display: inline-block; width: 60%; float: left;">
                            <div style="
                                 border: 1px solid #D6785E;
                                 width: 100%;
                                 height: 20px;
                                 ">
                                <div style="
                                     height: 100%;
                                     width: <?= !empty($rate3) ? intval(($rate3[0]['total'] / $totalRateCount[0]['total']) * 100) : 0; ?>%;
                                     background: red;
                                     "></div>
                            </div>
                        </div>
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="padding-left: 5px; font-size: 10px;"><?= !empty($rate3) ? $rate3[0]['total'] : 0; ?></span>
                        </div>

                    </div>

                    <?php
                    $rate2 = Yii::$app->db->createCommand('SELECT'
                                    . ' COUNT(rating) AS total FROM product_rating'
                                    . ' WHERE prod_id= ' . $productDetail[0]['id']
                                    . ' AND rating = 2 GROUP BY prod_id')
                            ->queryAll();
                    ?>
                    <div class="graph-row">
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="font-size: 12px;">2 stars</span>
                        </div>
                        <div style="display: inline-block; width: 60%; float: left;">
                            <div style="
                                 border: 1px solid #D6785E;
                                 width: 100%;
                                 height: 20px;
                                 ">
                                <div style="
                                     height: 100%;
                                     width: <?= !empty($rate2) ? intval(($rate2[0]['total'] / $totalRateCount[0]['total']) * 100) : 0; ?>%;
                                     background: red;
                                     "></div>
                            </div>
                        </div>
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="padding-left: 5px; font-size: 10px;"><?= !empty($rate2) ? $rate2[0]['total'] : 0; ?></span>
                        </div>

                    </div>


                    <?php
                    $rate1 = Yii::$app->db->createCommand('SELECT'
                                    . ' COUNT(rating) AS total FROM product_rating'
                                    . ' WHERE prod_id= ' . $productDetail[0]['id']
                                    . ' AND rating = 1 GROUP BY prod_id')
                            ->queryAll();
                    ?>
                    <div class="graph-row">
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="font-size: 12px;">1 stars</span>
                        </div>
                        <div style="display: inline-block; width: 60%; float: left;">
                            <div style="
                                 border: 1px solid #D6785E;
                                 width: 100%;
                                 height: 20px;
                                 ">
                                <div style="
                                     height: 100%;
                                     width: <?= !empty($rate1) ? intval(($rate1[0]['total'] / $totalRateCount[0]['total']) * 100) : 0; ?>%;
                                     background: red;
                                     "></div>
                            </div>
                        </div>
                        <div style="display: inline-block; width: 20%; float: left;">
                            <span style="padding-left: 5px; font-size: 10px;"><?= !empty($rate1) ? $rate1[0]['total'] : 0; ?></span>
                        </div>

                    </div>


                </div>
                <div class="col-md-4 btns">
                    <div style="margin-top: 25px; color: #645C5B; font-size: 16px; font-weight: bold; color: #A39F9E;">
                        <span> SHARE YOUR THOUGHTS </span>
                    </div>

                    <a class="btn review-btn" href="/product-review/<?= $productDetail[0]['id'] ?>">WRITE A REVIEW</a>
                    <a class="btn review-btn" href="/product-rate/<?= $productDetail[0]['id'] ?>">Rate A PRODUCT</a>
                    <?php
                    $this->registerCss(
                            ".review-btn {"
                            . "border: 1px solid #F0795F;"
                            . "margin: 10px;"
                            . "}"
                            . ".review-btn:hover {"
                            . "background: #F0795F;"
                            . "}"
                            . ".review-content {"
                            . "overflow-y: auto;"
                            . "}"
                    );
                    ?>
                </div>
            </div>

            <!-- Review block -->

            <div class="row" style="width: 100%; max-height: 50vh;">

                <?php
                $review = \common\models\ProductReview::find()
                        ->where([
                            'prod_id' => $productDetail[0]['id']
                        ])
                        ->all();
                ?>
                <?php
                $this->registerCss(
                        ".review-holder:hover {"
                        . "}"
                        . "div.review-holder>div.review-content {"
                        . "overflow: hidden;"
                        . "max-height: 41vh;"
                        . "}"
                        . "div.review-holder>div.review-content:hover {"
                        . "overflow-y: auto;"
                        . "box-shadow: 2px 2px #F0795F;"
                        . "}"
                );
                ?>
                <div class="review-holder" <?php if (!empty($review)) { ?>
                         style="width: 100%;
                         max-height: 50vh;
                         "
                     <?php } ?>>
                    <div class="section-title" style="
                         width: 100%;
                         padding-left: 10px;
                         padding-bottom: 5px;
                         font-size: 18px;
                         font-weight: bold;">
                        Reviews
                    </div>
                    <div class="review-content">
                        <?php
                        if (!empty($review)) {
                            foreach ($review as $key => $value) {
                                ?>
                                <div style="
                                     width: 90%;
                                     margin-left: auto;
                                     margin-right: auto;
                                     border-bottom: 1px solid #DADADA;
                                     padding: 5px;">
                                    <div style="padding: 5px;">
                                        <span style="
                                              float: left;
                                              font-size: 16px;
                                              font-weight: bold;
                                              "><?= $value['name'] ?></span>
                                        <span style="float: right;"><?= $value['review_date'] ?></span> <br>
                                    </div>
                                    <div style="
                                         width: 70%; margin-left: auto;
                                         margin-right: auto; padding-bottom: 10px;
                                         text-justify: auto;
                                         word-wrap: break-word;"
                                         >
                                        <p><?= $value['review'] ?></p>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>

            </div>

            <!--            Add div size of 970X250-->
            <?php
            $add970 = \common\models\DynamicAids::find()
                    ->where([
                        'size' => '970X250'
                    ])
                    ->all();
            foreach ($add970 as $ad970) {
                ?>
                <div class="under_review_add" style="width: 100%; padding-top: 10px; padding-bottom: 10px;">
                    <div style="width: 970px;
                         height: 250px; margin-left: auto;
                         margin-right: auto; padding-top: 10px;
                         padding-bottom: 10px;">
                         <?= $ad970->code ?>
                    </div>
                </div>
                <?php
                break;
            }
            ?>

            <!-- product specification div start -->
            <div>
                <div class="section-title">
                    <h2 class="title">Full Specification</h2>
                </div>
            <!--<table class="table">-->
                <?php
                $headCounter = 0;
                $spcMainTitle = common\models\ProductAttribute::find()
                        ->where([
                            'cat_id' => $productDetail[0]['cat_id'],
                            'parent' => 0
                        ])
                        ->all();
                if (!empty($spcMainTitle)) {
                    foreach ($spcMainTitle as $key => $value) {
                        $query = 'SELECT product_attribute.name,'
                                . ' product_att_value.value, product_att_value.unit,'
                                . ' product_att_value.detail'
                                . ' FROM product_attribute'
                                . ' RIGHT JOIN product_att_value'
                                . ' ON product_attribute.id = product_att_value.prod_att_id'
                                . ' WHERE product_att_value.prod_id = :id AND product_attribute.parent = :parent'
                                . ' ORDER BY product_attribute.name ASC';
                        $productSpecification = \Yii::$app->db->createCommand($query)
                                ->bindValue(':id', $productDetail[0]['id'])
                                ->bindValue(':parent', $value->id)
                                ->queryAll();
                        if ($headCounter == 4) {
                            foreach ($add970 as $ad970) {
                                ?>
                                <div class="under_review_add" style="width: 100%; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="width: 970px;
                                         height: 250px; margin-left: auto;
                                         margin-right: auto; padding-top: 10px;
                                         padding-bottom: 10px;">
                                         <?= $ad970->code ?>
                                    </div>
                                </div>
                                <?php
                                break;
                            }
                            $headCounter = 0;
                        }
                        if (!empty($productSpecification)) {
                            $check = TRUE;
                            ?>
                            <div style="
                                 font-size: 18px;
                                 font-weight: bold;
                                 color: #AAA;
                                 border-bottom: 1px solid #E6E6E6;
                                 "><?= $value->name ?></div>
                            <table class="table-borderless" style="
                                   margin-top: 10px;
                                   width: 100%;
                                   ">
                                <?php
                                $rearrange = [];
                                $tester = 0;
                                foreach($productSpecification as $speOne) {
                                    $ij = 0;
                                    do {
                                        if($tester == 0) {
                                            $rearrange[] = $speOne;
                                            $tester = 1;
                                        }
                                        if(strcmp($rearrange[$ij]['name'],$speOne['name']) == 0) {
                                            break;
                                        }
                                        if(($ij == (sizeof($rearrange)-1))) {
                                            $rearrange[] = $speOne;
                                        }
                                        $ij++;
                                    } while ($ij < sizeof($rearrange));
                                }
//                                print_r($rearrange);
                                foreach ($rearrange as $ke => $val) {
                                    ?>
                                <tr>
                                        <td style="
                                            font-size: 14px;
                                            font-weight: bold;
                                            width: 15%;
                                            word-wrap: break-word;
                                            "><?= $val['name'] ?></td>
                                        <td class="pull-left" style="
                                            min-height: 35px;
                                            padding-top: 10px;
                                            width: 85%;
                                            padding-left: 20px;
                                            word-wrap: break-word;
                                            ">
                                            <?= $val['value'] ?> 
                                            <?= $val['unit'] ?> 
                                            <?= $val['detail'] ?>
                                        </td>
                                    </tr>

                                <?php }
                                ?>
                            </table>
                            <?php
                            $headCounter++;
                        }
                    }
                }
                ?>
                <!--</table>-->
            </div>


            <!-- row -->
            <div class="row">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title">Related Products</h2>
                    </div>
                </div>
                <!-- section title -->
                <!-- section-title -->
                <div class="col-md-12">
                    <div class="pull-right">
                        <div class="product-slick-dots-6 custom-dots"></div>
                    </div>
                </div>
                <!-- /section-title -->
                <!-- Product Slick -->
                <div class="col-md-12">
                    <div class="row">
                        <div id="product-slick-6" class="product-slick">
                            <?php
                            foreach ($relatedProduct as $item) {
                                ?>
                                <!-- Product Single -->
                                <div class="product product-single">
                                    <div class="product-thumb">
                                        <?php
                                        $date_diff = intval(date_diff(date_create($item['release_date']), date_create(date('Y-m-d')))->format('%R%a'));
                                        if ($date_diff > 0 && $date_diff < 90) {
                                            ?>
                                            <div class="product-label">
                                                <span class="sale">New</span>
                                            </div>
                                        <?php } elseif ($date_diff < 0) { ?>
                                            <div class="product-label">
                                                <span class="sale">Upcoming</span>
                                            </div>
                                        <?php } ?>

                                        <a href="/product-detail/<?= $item['id'] ?>/<?= $item['brand_id'] ?>">
                                            <button class="main-btn quick-view"> 
                                                <i class="fa fa-search-plus"></i>
                                                Quick view
                                            </button>
                                        </a>
                                        <img src="uploads/<?= $item['path'] ?>" data-lazy="uploads/<?= $item['path'] ?>" alt="" 
                                             style="
                                             width: 50%;
                                             height: 20%;
                                             margin-left: auto;
                                             margin-right: auto;
                                             margin-top: 10px;
                                             ">
                                    </div>
                                    <div class="product-body">
                                        <h2 style="
                                            text-align: center;
                                            font-size: 18px;
                                            font-weight: bold;
                                            ">
                                            <a href="/product-detail/<?= $item['id'] ?>/<?= $item['brand_id'] ?>">
                                                <?= $item['name'] ?>
                                            </a>
                                        </h2>
                                        <h3 style="
                                            text-align: center;
                                            font-size: 16px;
                                            font-weight: bold;
                                            ">
                                            <span class="fa fa-dollar"></span>
                                            <span> <?= $item['price'] ?></span>
                                        </h3>
                                        <div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
                                            <?php
                                            if ($item['rating'] == 1) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 1 && $item['rating'] < 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 2 && $item['rating'] < 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 3 && $item['rating'] < 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif (($item['rating'] > 4) && ($item['rating'] < 5)) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <?php
                                            } elseif ($item['rating'] == 5) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                                <!-- /Product Single --> 
                            <?php } ?>                   
                        </div>
                    </div>
                </div>
                <!-- /Product Slick -->


            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /section -->

</div>