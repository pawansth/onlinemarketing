<?php
/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php
$this->registerCss(
        ".banner-top {"
        . "margin-top: 5px;"
        . "margin-bottom: 0;"
        . "}"
        . ".sub-cat-container {"
        . "width: 100%;"
        . "height: 38px;"
        . "overflow-x: hidden;"
        . "overflow-y: hidden;"
        . "position: relative;"
        . "display: block;"
        . "padding-top: 10px;"
        . "padding-bottom: 10px;"
        . "padding-left: 5px;"
        . "padding-right: 5px;"
        . "margin-bottom: 10px;"
        . "background: #f5f5f5;"
        . "}"
        . ".sub-cat-content>.sub-cat-item {"
        . "display: inline-block;"
        . "border-right: 2px solid #DADADA;"
        . "margin-left: 10px;"
        . "padding-right: 5px;"
        . "}"
        . ".cat_name {"
        . "color: black;"
        . "text-transform: uppercase;"
        . "}"
        . ".sub-cat-content {"
        . "width: 100%;"
        . "position: absolute;"
        . "top: 1;"
        . "left: 0.5;"
        . "z-index: 1;"
        . "overflow-x: hidden;"
        . "overflow-y: hidden;"
        . "white-space: nowrap;"
        . "}"
        . ".btn-left {"
        . "border: none;"
        . "background: none;"
        . "opicity: 0;"
        . "color: white;"
        . "z-index: 1;"
        . "}"
        . ".btn-right {"
        . "border: none;"
        . "background: none;"
        . "opicity: 0;"
        . "color: white;"
        . "z-index: 1;"
        . "}"
        . "/* width */
::-webkit-scrollbar {
    width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
box-shadow: inset 0 0 5px #f29c8a;
    background: #f1f1f1;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    background: #f29c8a;
    border-radius: 10px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #F0795F; 
}
"
        . "@media only screen and (max-width: 991px) {"
        . ".binner-aid {"
        . "display: none;"
        . "}"
        . ".sub-cat-container {"
        . "display: none;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 767px) {"
        . ".banner-aid {"
        . "display: none;"
        . "}"
        . ".sub-cat-container {"
        . "display: none;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 480px) {"
        . ".banner-aid {"
        . "display: none;"
        . "}"
        . ".sub-cat-container {"
        . "display: none;"
        . "}"
        . "}"
);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <meta name="description" content="<?= Html::encode($this->des) ?>">
        <meta name="keywords" content="<?= Html::encode($this->key) ?>">
        
        <link rel="shortcut icon" href="<?= Yii::getAlias('@web') ?>/uploads/log_marketing.ico" />
        <?php $this->head() ?>
    </head>
    <body id="style-1" style="background: #f5f5f5;">
        <?php $this->beginBody() ?>

        <header>
            <!-- top Header -->
            <div class="container-fluid banner-top">
                <?php
                $categories = common\provider\CategoriesProvider::categoriesList();
                if (!empty($categories)) {
                    ?>
                    <div class="sub-cat-container">
                        <div class="div-left pull-left" style="
                             height: 100%;
                             position: absolute;
                             cursor: pointer;
                             top: 0;
                             left: 0;
                             z-index: 3;
                             background: black;
                             opacity: 0.7;
                             padding-top: 5px;
                             display: none;
                             ">
                            <button class="btn-left glyphicon glyphicon-chevron-left"></button>
                        </div>
                        <div class="sub-cat-content">
                            <div style="display: inline-block;">
                                <span class="cat_name">Go To <span class="fa fa-arrow-right"></span></span>
                            </div>
                            <div class="sub-cat-item">
                                <span class="cat_name">
                                    <a href="<?= Yii::$app->homeUrl ?>">
                                        <span class="cat_name">Home</span>
                                    </a>
                                </span>
                            </div>
                            <?php
                            foreach ($categories as $cat) {
                                if ((sizeof($cat) <= 7)) {
                                    ?>
                                    <div class="sub-cat-item">
                                        <?php if (!empty($cat['link'])) { ?>
                                            <a href="<?= $cat['link'] ?>">
                                                <span class="cat_name"><?= $cat['name'] ?></span>
                                            </a>
                                        <?php } else { ?>
                                        <a href="<?= \yii\helpers\Url::to('/product-list/'.$cat['id']) ?>">
                                                <span class="cat_name"><?= $cat['name'] ?></span>
                                            </a>
                                        <?php }
                                        ?>
                                    </div>
                                    <?php
                                } else {
                                    recur($cat);
                                }
                            }
                            ?>
                        </div>
                        <div class="div-right pull-right" style="
                             height: 100%;
                             cursor: pointer;
                             position: absolute;
                             top: 0;
                             right: 0;
                             z-index: 3;
                             background: black;
                             opacity: 0.7;
                             padding-top: 5px;
                             display: none;
                             ">
                            <button class="btn-right glyphicon glyphicon-chevron-right"></button>
                        </div>
                    </div>
                    <?php
                }

                function recur($data) {
                    if ((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
                        ?>
                        <div class="sub-cat-item">
                            <?php if (!empty($data['link'])) { ?>
                                <a href="<?= $data['link'] ?>">
                                    <span class="cat_name"><?= $data['name'] ?></span>
                                </a>
                            <?php } else { ?>
                                <a href="<?= \yii\helpers\Url::to('/product-list/'.$data['id']) ?>">
                                    <span class="cat_name"><?= $data['name'] ?></span>
                                </a>
                            <?php }
                            ?>
                        </div>
                        <?php
                    }
                    if (sizeof($data) > 7) {
                        for ($i = 0; $i < sizeof($data) - 7; $i++) {
                            recur($data[$i]['node']);
                        }
                    }
                }
                ?>
                <div class="row">
                    <!-- Div for company logo and banner -->
                    <div class="col-md-4">
                        <a href="<?= Yii::$app->homeUrl ?>">
                            <img src="<?= Yii::getAlias("@web") ?>/uploads/log_marketing.gif" 
                                 alt="https://www.allgadgetreview.com" width="250" height="90"/>
                        </a>
                    </div>
                    <!-- Div for advertisement -->
                    <div class="col-md-8 banner-aid pull-right">
                        <?php
                        $bannerAid = common\models\DynamicAids::find()
                                ->where([
                                    'size' => '728X90'
                                ])
                                ->all();
                        foreach ($bannerAid as $ai) {
                            echo $ai->code;
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- /top Header -->

            <!-- header -->
            <div class="container-fluid">
                <div id="header">

                    <div class="pull-left">
                        <!-- Logo -->
                        <div class="header-logo">
                            <a class="logo" href="">
                            </a>
                        </div>
                        <!-- /Logo -->

                    </div>
                    <div class="pull-right">

                        <ul class="">
                            <li class="nav-toggle">
                                <button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
                            </li>
                            <!-- / Mobile nav toggle -->
                        </ul>
                    </div>
                </div>
                <!-- header -->
            </div>
            <!-- container -->
        </header>


        <div  id="not-for-main" class="container-fluid">
            <!-- NAVIGATION -->
            <div id="navigation">
                <!-- container -->
                <div class="container-fluid">
                    <div id="responsive-nav">
                        <!-- category nav -->
                        <div class="category-nav show-on-click">
                            <span class="category-header">Categories <i class="fa fa-list"></i></span>
                            <ul class="category-list" style="width: 340px;">
                                <?php
                                $indKey = 0;
                                foreach ($categories as $key => $cat) {
                                    ?>

                                    <?php if ((sizeof($cat) <= 7)) { ?>
                                <li>
                                            <?php if (!empty($cat['link'])) { ?>
                                    <a href="<?= $cat['link'] ?>">
                                            <span class="cat_name"><?= $cat['name'] ?></span>
                                                </a>
                                            <?php } else {
                                                ?>
                                                <a href="<?= \yii\helpers\Url::to('/product-list/'.$cat['id']) ?>">
                                                        <span class="cat_name"><?= $cat['name'] ?></span>
                                                    </a>
                                            <?php } ?>
                                        </li>
                                    <?php } else { ?>
                                        <li class="dropdown side-dropdown main-categories" style="text-transform: capitalize;">
                                            <a href="<?= \yii\helpers\Url::to('/product-list/'.$cat['id']) ?>">
                                                <span class="cat_name"><?= $cat['name'] ?></span>
                                                <i class="fa fa-angle-right"></i>
                                                </a>
                                            <?php
                                            $top = 0;
                                            if ($indKey != 0) {
                                                $top = - (100 * $indKey);
                                                $top .= '%';
                                            }
                                            ?>
                                            <div class="custom-menu" style="
                                                 top: <?= $top ?>;
                                                 ">
                                                     <?php recursive($cat); ?>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    $indKey++;
                                }
                                ?>
                            </ul>
                        </div>
                        <!-- /category nav -->

                        <?php
$this->registerCss(
        ".cat_name:hover {"
        . "color: #F8694A;"
        . "}"
        );
                        function recursive($data, $forTest = 0, $j = 0, $link = '') {
                            $forTest = $forTest;
                            $link = $link;
                            if ((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
                                ?>
                                <div <?= $forTest ? 'class=""' : 'class="col-md-4"' ?> 
                                <?=
                                $forTest ? 'style="'
                                        . 'font-size: 14px;'
                                        . 'font-weight: normal;'
                                        . 'text-transform: capitalize;'
                                        . '"' : 'style="
                                            text-transform: capitalize;
                             font-size: 14px;
                             font-weight: bold;
                             "'
                                ?> style="text-transform: capitalize;">
                                        <?php if ($j > 4 && $forTest) {
                                            ?>
                                    <div style="width: 100%;
                                         text-align: center;">
                                        <a class="cat-a" class="cat-a" href="<?= $link ?>">
                                            <span class="cat_name" style="color: #F8694A;">See All</span>
                                    </a>
                                    </div>
                                    </div>
                                    <?php
                                    return;
                                }

                                if (!empty($data['link'])) {
                                    ?>
                        <a class="cat-a" href="<?= $data['link'] ?>">
                                        <span class="cat_name"><?= $data['name'] ?></span>
                                    </a>
                                <?php } else { ?>
                                    <a class="cat-a" href="<?= \yii\helpers\Url::to('/product-list/'.$data['id']) ?>">
                                        <span class="cat_name"><?= $data['name'] ?></span>
                                    </a>
                                <?php }
                                ?>
                            </div>
                            <?php
                        } else {
                            if (!($data['parent'] == 0)) {
                                $forTest = 1;
                                $link = \yii\helpers\Url::to('/product-list/' . $data['id']);
                                ?>
                                <div class="col-md-4" style="
                                     font-size: 14px;
                                     font-weight: bold;
                                     text-transform: capitalize;
                                     ">
                                    <a class="cat-a" href="<?= \yii\helpers\Url::to('/product-list/'.$data['id']) ?>">
                                        <span class="cat_name"><?= $data['name']; ?></span>
                                        <i class="fa fa-caret-down"></i>
                                    </a>
                                <?php }
                                ?>
                                <?php
                            }
                            if (sizeof($data) > 7) {
                                ?>
                                <?php
                                for ($i = 0; $i < sizeof($data) - 7; $i++) {
                                    recursive($data[$i]['node'], $forTest, $i, $link);
                                    if (sizeof($data[$i]['node']) > 7) {
                                        echo '</div>';
                                    }
                                }
                                ?>
                                <?php
                            }
                        }
                        ?>

                        <!-- menu nav -->
                        <div class="menu-nav">
                            <span class="menu-header">Menu <i class="fa fa-bars"></i></span>
                            <ul class="menu-list">
                                <li><a href="<?= Yii::$app->homeUrl?>">Home</a></li>
                                <li class="dropdown"><a class="dropdown-toggle upcoming-main" data-toggle="dropdown" aria-expanded="true">Upcoming <i class="fa fa-caret-down"></i></a>
                                    <div class="custom-menu" style="height: 50vh; overflow-y: auto; overflow-x: hidden; top: 100%;">
                                        <ul class="">
                                            <?php
                                            $categories = common\provider\CategoriesProvider::categoriesList();
                                            if (!empty($categories)) {
                                                foreach ($categories as $cat) {
                                                    if (!empty($cat['link'])) {
                                                        ?>
                                            <li>
                                                <a class="drop-down-item" href="<?= $cat['link'] ?>">
                                                    <span class="cat_name"><?= $cat['name'] ?></span>
                                                            </a>
                                                        </li>
                                                        <hr>
                                                    <?php } ?>
                                                    <li>
                                                        <a class="drop-down-item" href="<?= \yii\helpers\Url::to('/upcoming/'.$cat['id']) ?>">
                                                            <span class="cat_name"><?= $cat['name'] ?></span>
                                                        </a>
                                                    </li>
                                                    <hr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <?php
                                            $this->registerCss(
                                                    ".drop-down-item:hover {"
                                                    . "cursor: pointer;"
                                                    . "margin: 0;"
                                                    . "padding: 0;  "
                                                    . "}"
                                                    . ".upcoming-main:hover {"
                                                    . "cursor: pointer;"
                                                    . "}"
                                            );
                                            ?>
                                        </ul>

                                    </div>
                                </li>
                                <li><a href="<?= \yii\helpers\Url::to(['viewproduct/smartphones']) ?>">Smartphones</a></li>
                                <li><a href="<?= Yii::$app->homeUrl .'about' ?>">About Us</a></li>
                                <li><a href="<?= Yii::$app->homeUrl .'contact' ?>">contact</a></li>
                                <!-- Search -->
                                <div class="header-search pull-right" aria-expanded="true">
                                    <form>
                                        <input 
                                            class="input search-input" 
                                            type="text" placeholder="Search products..." 
                                            style="
                                            background-image: url('css/search-icon-png-18.png');
                                            background-position: 8px 10px;
                                            background-repeat: no-repeat;
                                            padding-left: 28px;
                                            width: 100%;
                                            " autocomplete="off">

                                        <div class="livesearchproduct" style="
                                             height: 50vh;
                                             border: 1px solid #C5BEBC;
                                             width: 100%;
                                             overflow-x: auto;
                                             display: none;
                                             position: absolute;
                                             z-index: 100;
                                             box-shadow: 2px 2px #C5BEBC;
                                             "></div>
                                    </form>
                                </div>
                                <!-- /Search -->
                            </ul>

                        </div>

                        <!-- menu nav -->
                    </div>
                </div>
                <!-- /container -->
            </div>
        </div>
        <!-- /NAVIGATION -->


        <div class="container-fluid">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
            <?= Alert::widget() ?>
            <noscript>
            <div class="jumbotron" style="background: red; color: white; margin: 10px;">
                You don't have javascript enabled.  The site would not work properly.
            </div>
            </noscript>
            <?= $content ?>
            <?= \bluezed\scrollTop\ScrollTop::widget() ?>
        </div>

        <!-- FOOTER -->
        <footer id="footer" class="section section-grey">
            <!-- container -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- footer widget -->
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="footer">

                            <!-- footer social -->
                            <h3 class="footer-header">Join us on</h3>
                            <ul class="footer-social">
                                <li><a href="<?php // $footer->social_link1                                           ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?php // $footer->socail_link2                                         ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?php // $footer->social_link3                                           ?>"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="<?php // $footer->social_link4                                          ?>"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="<?php // $footer->social_link5                                           ?>"><i class="fa fa-pinterest"></i></a></li>
                            </ul>

                            <!-- /footer social -->
                        </div>
                    </div>

                    <!-- footer widget -->
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-header">Get to know us</h3>
                            <ul class="list-links">
                                <li><a href="<?= Yii::$app->homeUrl .'about'?>">About Us</a></li>
                                <li>
                                    <a href="<?= Yii::$app->homeUrl .'privacy-policy'?>">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="<?= \yii\helpers\Url::to('/site-map') ?>" target="_blank">
                                        Sitemap
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-header">Need Help</h3>
                            <ul class="list-links">
                                <li><a href="<?= \yii\helpers\Url::to(['/contact'])?>">Contact us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6"></div>
                    
                </div>
                <!-- /row -->
                <hr>
                <!-- row -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <!-- footer copyright -->
                        <div class="footer-copyright">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Developed By:- <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="https://www.facebook.com/anil.shrestha.9406417" target="_blank">Pawan Shrestha</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                        <!-- /footer copyright -->
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </footer>
        <!-- /FOOTER -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
<?php
$this->registerCssFile('@web/css/site.css');
?>