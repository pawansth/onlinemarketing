<?php
/* @var $this yii\web\View */

$this->title = Yii::$app->name;

$keyDes = \common\models\KeyDes::findOne(['ind' => 'home']);

$this->des = $keyDes->description;
$this->key = $keyDes->keyword;

$this->registerCss(
        ".slider-aids {"
        . "float: left;"
        . "padding-left: 10px;"
        . "width: 75%;"
        . "}"
        . ".banner {"
        . "height: 85vh;"
        . "}"
        . ".slider-side-aids {"
        . "padding-left: 20px;"
        . "float: left;"
        . "width: 25%;"
        . "}"
        . ".image-aids-div {"
        . "background: #e0e0e0;"
        . "width: 100%;"
        . "height: 38%"
        . "margin-top: 10px;"
        . "padding-top: 20px;"
        . "padding-bottom: 20px;"
        . "display: block;"
        . "}"
        . ".aid-online-main {"
        . "background: #FFFFFF;"
        . "width: 100%;"
        . "padding-top: 20px;"
        . "padding-bottom: 20px;"
        . "}"
        . ".aid-online-sub {"
        . "width: 970px;"
        . "height: 250px;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "}"
        . "@media only screen and (max-width: 991px) {"
        . ".slider-aids {"
        . "padding-left: 2px;"
        . "width: 60%;"
        . "}"
        . ".slider-side-aids {"
        . "padding-left: 10px;"
        . "width: 40%;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 971px){"
        . ".aid-online-main {"
        . "display: none;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 767px) {"
        . ".slider-aids {"
        . "width: 100%;"
        . "}"
        . ".banner {"
        . "height: 50vh;"
        . "}"
        . ".slider-side-aids {"
        . "display: none;"
        . "}"
        . ".aid-online-main {"
        . "display: none;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 480px) {"
        . ".slider-aids {"
        . "width: 100%;"
        . "}"
        . ".banner {"
        . "height: 50vh;"
        . "}"
        . ".slider-side-aids {"
        . "display: none;"
        . "}"
        . ".image-aids-div {"
        . "height: 33%;"
        . "}"
        . ".aid-online-sub {"
        . "width: 100%;"
        . "}"
        . ".aid-online-main {"
        . "display: none;"
        . "}"
        . "}"
);
?>
<div class="site-index" style="background: #FFFFFF;">
    <div class="row" style="margin-top: 20px; padding-left: 20px; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">
        <div class="slider-aids">
            <!-- home slick -->
            <div id="home-slick">
                <?php
                $mainslider = common\models\Slider::find()->all();
                foreach ($mainslider as $slider) {
                    ?>
                    <!-- banner -->
                    <div class="banner banner-1">
                        <img src="uploads/<?= $slider->path ?>" alt="" class="slider-img" style="
                             width: 100%;
                             height: 100%;
                             ">
                        <div class="banner-caption text-center">
                            <h1><?= $slider->title ?></h1>
                            <a class="primary-btn" href="<?= $slider->link ?>">GO</a>
                        </div>
                    </div>
                    <!-- /banner -->
                <?php } ?>

            </div>
            <!-- /home slick -->
        </div>
        <div class="slider-side-aids">
            <?php
            $aid = \common\models\DynamicAids::find()
                    ->where([
                        'size' => '300X600'
                    ])
                    ->all();
            foreach ($aid as $ad) {
                echo $ad->code;
                break;
            }
            ?>
        </div>
    </div>

    <!-- Home page content -->
    <?php
    $aidLastIndex = 0;
    $imageAids = \common\models\ImgAids::find()
            ->all();
    if (!empty($imageAids)) {
        ?>
        <div class="image-aids-div">
            <?php
            $arrayCount = 1;
            for ($i = $aidLastIndex; $i < sizeof($imageAids); $i++) {
                $aidLastIndex = $i;
                if (($arrayCount > 3) || ($aidLastIndex == (sizeof($imageAids) - 1))) {
                    $arrayCount = 0;
                    break;
                }
                ?>
                <div style="
                     width: 30%;
                     height: 100%;
                     display: inline-block;
                     margin-left: <?php
                     if ($arrayCount == 1)
                         echo '2%';
                     else
                         echo '0';
                     ?>;
                     margin-right:2%;
                     ">
                    <a href="<?= $imageAids[$i]->link ?>" 
                       target="_blank"
                       style="display: inline-block">
                        <img src="uploads/<?= $imageAids[$i]->path ?>" style="width: 100%; height: 100%;">
                    </a>
                </div>
                <?php
                $arrayCount++;
            }
            ?>
        </div>
    <?php } ?>

    <div>

        <?php
        $cat_count = 0;
        $categories = common\provider\CategoriesProvider::categoriesList();
        foreach ($categories as $cat) {
            if ($cat_count == 2 || $cat_count == 4) {
                if (!empty($imageAids)) {
                    if ($aidLastIndex != (sizeof($imageAids) - 1)) {
                        ?>
                        <div class="image-aids-div">
                            <?php
                            $arrayCount = 1;
                            for ($i = $aidLastIndex; $i < sizeof($imageAids); $i++) {
                                $aidLastIndex = $i;
                                if (($arrayCount > 3) || ($aidLastIndex == (sizeof($imageAids) - 1))) {
                                    $arrayCount = 0;
                                    break;
                                }
                                ?>
                                <div style="
                                     width: 30%;
                                     height: 100%;
                                     display: inline-block;
                                     margin-left: <?php
                                     if ($arrayCount == 1)
                                         echo '2%';
                                     else
                                         echo '0';
                                     ?>;
                                     margin-right:2%;
                                     ">
                                    <a href="<?= $imageAids[$i]->link ?>" 
                                       target="_blank"
                                       style="display: inline-block">
                                        <img src="uploads/<?= $imageAids[$i]->path ?>" style="width: 100%; height: 100%;">
                                    </a>
                                </div>
                                <?php
                                $arrayCount++;
                            }
                            ?>
                        </div>
                        <?php
                    }
                }
            }

            if ($cat_count == 5) {
                $dynamicAidBottom = \Yii::$app->db->createCommand(
                                "SELECT * FROM dynamic_aids WHERE size = '970X250'"
                        )
                        ->queryAll();
                if (!empty($dynamicAidBottom)) {
                    foreach ($dynamicAidBottom as $da) {
                        ?>
                        <div class="aid-online-main">
                            <div class="aid-online-sub">
                                <?= $da['code']; ?>
                            </div>
                        </div>
                        <?php
                        break;
                    }
                }
                $cat_count = 0;
            }

            if ((sizeof($cat) <= 7)) {
                createContent($cat['id'], $cat['name']);
            } else {
                recursives($cat);
            }
            $cat_count++;
        }

        function recursives($data) {
            if ((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
                createContent($data['id'], $data['name']);
            }
            for ($i = 0; $i < sizeof($data) - 7; $i++) {
                recursives($data[$i]['node']);
            }
        }

        function createContent($id, $name) {
            $query = 'SELECT product.*, table1.rating, table2.path FROM product'
                    . ' LEFT JOIN (SELECT SUM(rating) / COUNT(rating) AS rating,'
                    . ' prod_id  FROM product_rating GROUP BY (prod_id)) table1'
                    . ' ON product.id = table1.prod_id LEFT JOIN (SELECT * FROM'
                    . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                    . ' = product.id'
                    . ' WHERE product.sub_cat_id = :sub_id';
            $relatedProduct = \Yii::$app->db->createCommand($query)
                    ->bindValue(':sub_id', $id)
                    ->queryAll();
            if (empty($relatedProduct)) {
                return;
            }
            ?>
            <div class="row" style="padding-right: 20px; padding-left: 20px;">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <div style="width: 100%; display: block;">
                            <div style="width: 73%; height: 33px; display: inline-block; overflow: hidden;
                                 white-space: nowrap; text-overflow: ellipsis;">
                                <h2 class="title"><?= $name ?></h2>
                            </div>
                            <div style="width: 25%; display: inline-block;">
                                <a 
                                    class="btn btn-primary" 
                                    style="background: #F0795F;
                                    border: 1px solid #F0795F;
                                    float: right;"
                                    href="<?= Yii::$app->homeUrl ?>product-list/<?= $id ?>">
                                    View All 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- section title -->
                <!-- section-title -->
                <div class="col-md-12">
                    <div class="pull-right">
                        <div class="product-slick-dots-6 custom-dots"></div>
                    </div>
                </div>
                <!-- /section-title -->
                <!-- Product Slick -->
                <div class="col-md-12">
                    <div class="row">
                        <div id="product-slick-6" class="product-slick" style="width: 100%;">
                            <?php
                            foreach ($relatedProduct as $item) {
                                ?>
                                <!-- Product Single -->
                                <div class="product product-single">
                                    <div class="product-thumb">
                                        <?php
                                        $date_diff = intval(date_diff(date_create($item['release_date']), date_create(date('Y-m-d')))->format('%R%a'));
                                        if ($date_diff > 0 && $date_diff < 90) {
                                            ?>
                                            <div class="product-label">
                                                <span class="sale">New</span>
                                            </div>
                                        <?php } elseif ($date_diff < 0) { ?>
                                            <div class="product-label">
                                                <span class="sale">Upcoming</span>
                                            </div>
                                        <?php } ?>

                                        <a href="/product-detail/<?= $item['id'] ?>/<?= $item['brand_id'] ?>">
                                            <button class="main-btn quick-view"> 
                                                <i class="fa fa-search-plus"></i>
                                                Quick view
                                            </button>
                                        </a>
                                        <img src="uploads/<?= $item['path'] ?>" data-lazy="uploads/<?= $item['path'] ?>" alt="" style="
                                             width: 50%;
                                             height: 20%;
                                             margin-left: auto;
                                             margin-right: auto;
                                             margin-top: 10px;
                                             ">
                                    </div>
                                    <div class="product-body">
                                        <h2 style="
                                            text-align: center;
                                            font-size: 18px;
                                            font-weight: bold;
                                            ">
                                            <a href="/product-detail/<?= $item['id'] ?>/<?= $item['brand_id'] ?>">
                                                <?= $item['name'] ?>
                                            </a>
                                        </h2>
                                        <h3 style="
                                            text-align: center;
                                            font-size: 16px;
                                            font-weight: bold;
                                            ">
                                            <span class="fa fa-dollar"></span>
                                            <span> <?= $item['price'] ?></span>
                                        </h3>
                                        <div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
                                            <?php
                                            if ($item['rating'] == 1) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 1 && $item['rating'] < 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 2 && $item['rating'] < 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 3 && $item['rating'] < 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif (($item['rating'] > 4) && ($item['rating'] < 5)) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <?php
                                            } elseif ($item['rating'] == 5) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                                <!-- /Product Single --> 
                            <?php } ?>                   
                        </div>
                    </div>
                </div>
                <!-- /Product Slick -->


            </div>
        <?php }
        ?>

    </div>

</div>
