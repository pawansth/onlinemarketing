<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss(
        ".site-about {"
        . "background: #FFFFFF;"
        . "padding-top: 20px;"
        . "padding-bottom: 20px;"
        . "}"
        . ".parent-container {"
        . "width: 100%;"
        . "min-height: 100vh;"
        . "}"
        . ".contact-form {"
        . "width: 75%;"
        . "padding-left: 10px;  "
        . "height 100%;"
        . "}"
        . ".gooleAdd {"
        . "margin-left: 10px;"
        . "padding-top: 10px;"
        . "padding-bottom: 10px;"
        . "}"
        . "@media only screen and (max-width: 991px) {"
        . ".gooleAdd {"
        . "display: none;"
        . "}"
        . ".contact-form {"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 767px) {"
        . ".gooleAdd {"
        . "display: none;"
        . "}"
        . ".contact-form {"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "}"
        . "}"
        . "@media only screen and (max-width: 480px) {"
        . ".gooleAdd{"
        . "display: none;"
        . "}"
        . ".contact-form {"
        . "width: 90%;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "}"
        . "}"
);
?>
<div class="site-about">
    <div class="row">
        <div class="col-md-4 gooleAdd">
        <?php 
        $dyAdd = common\models\DynamicAids::find()
                ->where([
                    'size' => '300X600'
                ])
                ->all();
                foreach($dyAdd as $adds) {
                    echo $adds->code;
                    break;
                }
        ?>
    </div>
    <div class="col-md-7">
         <div class="parent-container">

        <div class="contact-form">

            <!-- Write about your site here -->

        </div>

    </div>
    </div>
    </div>
</div>
