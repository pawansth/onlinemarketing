<?php

header("Content-Type: application/xml; charset=utf-8"); 
echo '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
echo '<?xml-stylesheet type="text/css" href="http://localhost/web%20projects/marketing/frontend/web/css/xmlStyle.css"?>'.PHP_EOL;
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"'
. ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
        . ' xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9'
        . ' http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' .PHP_EOL; 
 
function urlElement($url, $name) {
echo '<url>'.PHP_EOL; 
echo '<loc><![CDATA['.$url.']]></loc>'. PHP_EOL;
echo '<name>'.$name.'</name>'.PHP_EOL;
echo '<changefreq>weekly</changefreq>'.PHP_EOL; 
echo '</url>'.PHP_EOL;
} 

 $categories = common\provider\CategoriesProvider::categoriesList();
 if (!empty($categories)) {
     foreach ($categories as $cat) {
         echo '<menu-list>'.PHP_EOL;
         echo '<menu-title>'.$cat['name'].'</menu-title>'.PHP_EOL;
         if ((sizeof($cat) <= 7)) {
             if (!empty($cat['link'])) {
                 urlElement($cat['link'], $cat['name']);
             } else {
    urlElement(Yii::$app->homeUrl.'?r=product/product-list&id='.$cat['id'], $cat['name']);
             }
         } else {
             rec($cat);
         }
         echo '</menu-list>'.PHP_EOL;
     }
 }
 
 function rec($data) {
     if ((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
         if (!empty($data['link'])) {
             urlElement($data['link'], $data['name']);
         } else {
             urlElement(Yii::$app->homeUrl.'?r=product/product-list&id='.$data['id'], $data['name']);
         }
     }
     if (sizeof($data) > 7) {
                        for ($i = 0; $i < sizeof($data) - 7; $i++) {
                            rec($data[$i]['node']);
                        }
                    }
 }
 
echo '</urlset>'.PHP_EOL; 