<?php


namespace frontend\controllers;

use common\models\Categories;

/**
 * Description of ParentCategoriesProvider
 *
 * @author pawan
 */
class ParentCategoriesProvider {
    static public function parentCat($id) {
        /*
         * finding parent categories using reccurrsive method
         */
        return self::recurssive($id);
    }
    
    /*
     * Recurssive function to find parent
     * categories
     */
    static private function recurssive($id) {
        $cat = Categories::find()
                ->where([
                    'id' => $id
                ])
                ->one();
        if(empty($cat)) {
            return $id;
        }
        if(! $cat->parent) {
            return $cat->id;
        }
        return self::recurssive($cat->parent);
    }
}
